<?php
	$page = isset($_GET['page']) ? $_GET['page'] : 0;
	$newsPerPage = 5;
	$page *= $newsPerPage;
	$sql="SELECT n.title, n.image, n.text, n.date, u.login FROM news AS n INNER JOIN users AS u ON n.author = u.id ORDER BY n.id DESC LIMIT $newsPerPage OFFSET $page";
	if($rezultat=$sql_conn->query($sql))
	{
		while($row = $rezultat->fetch_assoc())
		{
			echo '<div>';
			echo '<h2>'.$row['title'].'</h2>';
			if($row['image']!=null)
				echo '<div style="display: table-cell;"><img src="'.$row["image"].'" width=96 /></div>';
			echo '<div style="margin-left:30px; display: table-cell; vertical-align: top;">'.$row['text'].'</div>';
			echo '<div style="text-align: right; color:#848484">'.$row['date'].' <i>~'.$row['login'].'</i>'.'</div>';
			echo '<hr/>';
			echo '</div>';
		}
	}
	else{
		echo $sql_conn->error;
		echo "<hr />";
	}
?>
<div style="text-align:center">
	<?php 
		$sql = "SELECT COUNT(*) as count FROM news";
		if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->fetch_assoc()['count'];
			$pageCount = ceil($count/$newsPerPage);
			for($i = 0; $i < $pageCount; $i++){
				echo '<a href="?news&page='.$i.'">['.($i+1).']</a> ';
			}
		}else{
			die($sql_conn->error);
		}
	?>
</div>