<table class="championsList" style="margin: 0 auto;">
    <thead>
		<tr>
			<th>L.p</th>
			<th>Nick</th>
			<th>Skin</th>
			<th>Poziom</th>
			<th>Ostatnio zalogowany</th>
		</tr>
    </thead>
    <tbody>
        <?php
            $page = isset($_GET['page']) ? $_GET['page'] : 0;
            $championsPerPage = 20;
            $page *= $championsPerPage;
            $sql="SELECT c.id, c.name, c.level, s.image, c.lastOnline FROM champions AS c JOIN skins AS s ON c.skin = s.id ORDER BY c.level desc LIMIT $championsPerPage OFFSET $page";
            if($rezultat=$sql_conn->query($sql))
            {
                $i = 1;
                while($row = $rezultat->fetch_assoc())
                {
                    echo '<tr>';
                    echo '<td>'.$i++.'</td>';
                    echo '<td>'.$row['name'].'</td>';
                    echo '<td>'.'<div class="championImage" style="background-image: url('.$row['image'].')"></div>'.'</td>';
                    echo '<td>'.$row['level'].'</td>';
                    echo '<td>'.timeDif($row['lastOnline'], $row['id']).'</td>';
                    echo '</tr>';
                }
            }
            else{
                echo $sql_conn->error;
                echo "<hr />";
            }
        ?>
    </tbody>
</table>
<hr/>
<div style="text-align: center">
<?php
$sql = "SELECT COUNT(*) as count FROM champions";
if($rezultat=$sql_conn->query($sql)){
    $count = $rezultat->fetch_assoc()['count'];
    $pageCount = ceil($count/$championsPerPage);
    for($i = 0; $i < $pageCount; $i++){
        echo '<a href="?ranking&page='.$i.'">['.($i+1).']</a> ';
    }
}
?>
</div>