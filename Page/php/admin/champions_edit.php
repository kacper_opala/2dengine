<?php
$rezult = new stdClass();
$rezult->errorCode = 0;
$champion = $_GET['edit'];
$sql = "SELECT 
c.name, 
c.user,
c.level, 
c.health, 
c.mana, 
c.experience, 
ST_X(c.position) as positionX, 
ST_Y(c.position) as positionY, 
c.strength, 
c.dexterity, 
c.intelligence, 
c.gold,
c.created,

s.id as skinId,
s.image as skin, 
ST_X(s.size) as skinWidth,
ST_Y(s.size) as skinHeight,

m.id as mapId, 
m.name as mapName, 
m.background as mapBackground, 
m.battleBackground as mapBattleBackground, 
m.layer1 as mapLayer1, 
m.layer2 as mapLayer2, 
m.collisions as mapCollisions, 
ST_X(m.size) as mapWidth,
ST_Y(m.size) as mapHeight,
st.maxHp as maxHp, st.maxMp as maxMp, st.maxXp as maxXp
FROM champions as c
INNER JOIN skins as s ON c.skin = s.id
INNER JOIN maps as m ON c.map = m.id
LEFT JOIN statsperlevel as st ON c.level = st.id
WHERE c.id=$champion";
if($rezultat=$sql_conn->query($sql)){
$count = $rezultat->num_rows;
if($count == 1){
    $row = $rezultat->fetch_assoc();
    $mapId = $row['mapId'];
    //Load champion
    $rezult->champion = array(
        "name" => $row['name'],
        "user" => intval($row['user']),
        "level" => intval($row['level']),
        "health" => floatval($row['health']),
        "mana" => floatval($row['mana']),
        "experience" => intval($row['experience']),
        "position" => array( "x"=>intval($row['positionX']), "y"=>intval($row['positionY'])),
        "strength" => intval($row['strength']),
        "dexterity" => intval($row['dexterity']),
        "intelligence" => intval($row['intelligence']),
        "maxHp" => intval($row['maxHp']),
        "maxMp" => intval($row['maxMp']),
        "maxXp" => intval($row['maxXp']),
        "gold" => intval($row['gold']),
        "skin" => array(
            "id" => $row['skinId'],
            "image" => $row['skin'],
            "size" => array( "width"=>intval($row['skinWidth']), "height"=>intval($row['skinHeight']))
        ),
        "created" => $row['created']
    );
    //Load map
    $rezult->map = array(
        "name" => $row['mapName'],
        "background" => $row['mapBackground'],
        "battleBackground" => $row['mapBattleBackground'],
        "layer1" => $row['mapLayer1'],
        "layer2" => $row['mapLayer2'],
        "collisions" => json_decode($row['mapCollisions']),
        "size" => array( "width"=>intval($row['mapWidth']), "height"=>intval($row['mapHeight'])),
    );
    //Load friends
    $sql = "SELECT c.name, c.level, m.name as map, f.status, f.CreationTime
    FROM friends as f
    JOIN champions as c ON c.id = f.AddresseeId
    JOIN maps as m ON m.id = c.map
    WHERE f.RequesterId = $champion
    UNION
    SELECT c.name, c.level, m.name, f.status, f.CreationTime
    FROM friends as f
    JOIN champions as c ON c.id = f.RequesterId
    JOIN maps as m ON m.id = c.map
    WHERE f.AddresseeId = $champion";
    if($rezultat=$sql_conn->query($sql)){
        $rezult->friends = [];
        while($row = $rezultat->fetch_assoc()){
            $rezult->friends[] = $row;
        }
    }else{
        $rezult->error = $sql_conn->error;
        $rezult->errorCode = 3;
        die(json_encode($rezult));
    }
    //Load quests
    $sql = "SELECT q.name, q.value, q.status from quests as q WHERE champion=$champion";
    if($rezultat=$sql_conn->query($sql)){
        include('php/game/requestParser.php');
        $rezult->quests = [];
        while($row = $rezultat->fetch_assoc()){
            $rezult->quests[] = array(
                "name"=> $row["name"],
                "value"=> requestParser::parse($row['value'], $champion),
                "status"=> $row["status"],
            );
        }
    }else{
        $rezult->error = $sql_conn->error;
        $rezult->errorCode = 3;
        die(json_encode($rezult));
    }
    //Load equipment
    $sql = "SELECT i.name, i.icon, i.type, i.stack, i.attributes, i.value, e.count, e.active 
    FROM equipment as e
    JOIN items as i ON i.id = e.itemId
    WHERE champion=$champion";
    if($rezultat=$sql_conn->query($sql)){
        $rezult->equipment = [];
        while($row = $rezultat->fetch_assoc()){
            $rezult->equipment[] = array(
                "name"=> $row['name'],
                "icon"=> $row['icon'],
                "type"=> $row['type'],
                "stack"=> intval($row['type']),
                "attributes"=> json_decode($row['attributes']),
                "value"=> intval($row['value']),
                "active"=> boolval($row['active']),
            );
        }
    }else{
        $rezult->error = $sql_conn->error;
        $rezult->errorCode = 3;
        die(json_encode($rezult));
    }
    //Load switches
    $sql = "SELECT s.name, s.value FROM switches as s WHERE champion=$champion";
    if($rezultat=$sql_conn->query($sql)){
        $rezult->switches = [];
        while($row = $rezultat->fetch_assoc()){
            $rezult->switches[] = array(
                "name"=> $row['name'],
                "value"=> json_decode($row['value']),
            );
        }
    }else{
        $rezult->error = $sql_conn->error;
        $rezult->errorCode = 3;
        die(json_encode($rezult));
    }
    //Load skils
    $sql = "SELECT s.name, s.icon, s.action, s.cost
    FROM championskils as cs
    JOIN skils as s ON s.id = cs.skill
    WHERE cs.champion=$champion";
    if($rezultat=$sql_conn->query($sql)){
        $rezult->skils = [];
        while($row = $rezultat->fetch_assoc()){
            $rezult->skils[] = array(
                "name"=> $row['name'],
                "icon"=> $row['icon'],
                "action"=> json_decode($row['action']),
                "cost"=> intval($row['cost']),
            );
        }
    }else{
        $rezult->error = $sql_conn->error;
        $rezult->errorCode = 3;
        die(json_encode($rezult));
    }
}
}else{
    $rezult->error = $sql_conn->error;
    $rezult->errorCode = 3;
    die(json_encode($rezult));
}
//echo json_encode($rezult);
?>
<style>
    .mainBox{
        width: 100%;
        height: 100%;
        text-align: center;
        border-spacing: 10px;
    }
    .mainBox td{
        border: solid 1px;
        background-color: white;
    }
</style>
<script src="js/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
<link href="js/msdropdown/css/dd.css" rel="stylesheet" type="text/css" />
<script>
$(function() {
    $( "#skin" ).msDropDown({visibleRows:1.5});
    $('.mainBox td').click(()=>{
        console.log("XD");
    });
});
</script>
<table class="mainBox">
    <tbody>
        <tr height=32>
            <td colspan="5">Data Utworzenia: <?= $rezult->champion['created'] ?></td>
        </tr>
        <tr>
            <td colspan="2" style="width:25%;">Zmiana Nicku <input type="text" class="smallInput" placeholder="Nick" value="<?= $rezult->champion['name']?>"/><button class="button2">Zapisz</button></td>
            <td style="width:25%;">Zmiana Użytkownika <input type="text" class="smallInput" placeholder="User id" value="<?= $rezult->champion['user']?>"/><button class="button2">Zapisz</button></td>
            <td rowspan="2" style="width:25%; height:50%">
                <select name="skin" id="skin" placeholder="Skin" >
                    <?php
                        $sql = "SELECT id, image FROM skins";
                        if($rezultat=$sql_conn->query($sql)){
                            while($row = $rezultat->fetch_assoc()){
                                $selected = $rezult->champion['skin']['id'] == $row['id'] ? "selected":"";
                                echo "<option {$selected} value='{$row['id']}' data-image='{$row['image']}'></option>";
                            }
                        }else{
                            die($sql_conn->error);
                        }
                    ?>
                </select><br/>
                <button class="button2">Zapisz</button>
            </td>
            <td rowspan="2" style="width:25%">#EdycjaPrzełączników</td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="text-align:left">
                    <tr>
                        <td>Poziom<input type="text" class="smallInput" placeholder="Poziom" value="<?= $rezult->champion['level']?>"/></td>
                        <td>Doświadczenie<input type="text" class="smallInput" placeholder="Doświadczenie" value="<?= $rezult->champion['experience']?>"/></td>
                        <td>Złoto<input type="text" class="smallInput" placeholder="Doświadczenie" value="<?= $rezult->champion['gold']?>"/></td>
                    </tr>
                    <tr>
                        <td>Siła<input type="text" class="smallInput" placeholder="Siła" value="<?= $rezult->champion['strength']?>"/></td>
                        <td>Zręczność<input type="text" class="smallInput" placeholder="Zręczność" value="<?= $rezult->champion['dexterity']?>"/></td>
                        <td>Inteligencja<input type="text" class="smallInput" placeholder="Inteligencja" value="<?= $rezult->champion['intelligence']?>"/></td>
                    </tr>
                    <tr>
                        <td>Poziom zdrowia<input type="text" class="smallInput" placeholder="Zdrowie" value="<?= $rezult->champion['health']?>"/></td>
                        <td>Poziom many<input type="text" class="smallInput" placeholder="Mana" value="<?= $rezult->champion['mana']?>"/></td>
                        <td><button class="button2">Zapisz</button></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" rowspan="4" style="width:50%; height:50%">#Mapa</td>
            <td rowspan="2">#Eq</td>
            <td rowspan="2">#Skils</td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td rowspan="2">#Znajomi</td>
            <td rowspan="2">#Zadania</td>
        </tr>
        <tr>
        </tr>
    </tbody>
</table>
<?php

?>