<?php
	$edit = $_GET['edit'];
	if(isset($_POST['updateUser'])){
		$login = $_POST['login'];
		$password = $_POST['password'];
		$mail = $_POST['mail'];
		$sql = "UPDATE users SET login='$login', mail='$mail' ";
		if(!empty($password)){
			$password = md5($password);
			$sql .= ",password='$password' ";
		}
		if(isset($_POST['access']) && $_SESSION['login']['access']=='admin'){
			$access = $_POST['access'];
			$sql .= ", access='$access' ";
		}
		$sql .=  "WHERE id=$edit";
		if(!$rezultat=$sql_conn->query($sql)){
			die($sql_conn->error);
		}
	}

	$sql = "SELECT id, login, mail, created, access FROM users WHERE id=$edit";
	if($rezultat=$sql_conn->query($sql)){
		$row = $rezultat->fetch_assoc();
	}else{
		die($sql_conn->error);
	}
?>
<style>
.table input, .table select{
	width:100%;
}
</style>
<form action="" method="POST">
	<table class="table">
		<tr>
			<td><input type="text" name="login" value="<?= $row['login']?>" placeholder="Zmień login" /></td>
			<td><input type="password" name="password" value="" placeholder="Zmień hasło" /></td>
			<td><input type="mail" name="mail" value="<?= $row['mail']?>" placeholder="Zmień @-mail" /></td>
			<td><?= $row['created']?></td>
			<td>
				<select placeholder="Poziom dostępu" name="access" <?= $_SESSION['login']['access'] != 'admin' || $edit==$_SESSION['login']['id'] ? 'disabled' : '' ?>>
					<?php 
					$enum = get_enum_values('users','access');
					foreach($enum as $val){
						echo "<option value='$val' ".($row['access']==$val ? 'selected' : '').">$val</option>";
					}
					?>
				</select>
			</td>
			<?php
				if($_SESSION['login']['access'] == 'admin' && $_GET['edit'] !=1 && $_GET['edit'] !=$_SESSION['login']['id'])
					echo '<td><a href="?admin&site=users&deleteUser='.$_GET['edit'].'"/>Usuń</a></td>';
			?>
			<td>
				<button type="submit" name="updateUser" class="button2">Zapisz</button>
			</td>
		</tr>
	</table>
</form>
<?php
	$showUserChampions = true;
	include('php/admin/champions_get.php');
?>