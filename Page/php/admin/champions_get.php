<?php
if(isset($_GET['deleteChampion'])){
	$delete = $_GET['deleteChampion'];
	$sql = "DELETE FROM champions WHERE id=$delete";
	if($rezultat=$sql_conn->query($sql)){
		if(isset($_GET['redirect']))
			header('Location: ?admin&site=users&edit'.$_GET['redirect']);
		else
			header('Location: ?admin&site=champions');
	}else{
		die($sql_conn->error);
	}
}
if(!isset($showUserChampions)){
	echo '<div style="text-align:right"><form action="">
	<input type="hidden" name="admin"/>
	<input type="hidden" name="site" value="champions"/>';
	if(isset($_GET['page'])){
		echo '<input type="hidden" name="page" value="'.$_GET['page'].'"/>';
	}
	echo '<input type="text" name="search" placeholder="Szukaj" autocomplete="off" '.(isset($_GET['search']) ? 'value="'.$_GET['search'].'"':'').'/></form></div>';
}
?>
<hr/>
<table class="table">
	<thead>
		<tr>
			<th>L.p</th>
			<th>Nick</th>
			<?= !isset($showUserChampions)? "<th>Użytkownik</th>": ""?>
			<th>Skin</th>
			<th>Poziom</th>
			<th>Pozycja</th>
			<th>Data utworzenia</th>
			<th>Ostatnio zalogowany</th>
			<th>Szczegóły</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$usersPerPage = 10;
			$page *= $usersPerPage;
			$sql = "SELECT c.id, u.login, c.name, c.level, 
				s.image as skin,
				m.name as map, m.layer1 as mapImg, ST_X(c.position) as xPos, ST_Y(c.position) as yPos, c.created, c.lastOnline
				FROM champions as c 
				JOIN users as u ON u.id = c.user
				JOIN skins as s ON c.skin = s.id
				JOIN maps as m ON m.id = c.map ";
			if(isset($_GET['search'])){
				$name = $_GET['search'];
				$sql .= "WHERE c.name LIKE '%$name%' ";
			}else if(isset($showUserChampions)){
				$edit = $_GET['edit'];
				$sql .= "WHERE u.id=$edit ";
			}
			$sql .= "ORDER BY c.id ASC LIMIT $usersPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->num_rows;
					if($count > 0){
						$i = 1;
						while($row = $rezultat->fetch_assoc()){
							echo '<tr>';
							echo '<td>'.$i++.'</td>';
							echo '<td>'.$row['name'].'</td>';
							if(!isset($showUserChampions))
								echo '<td>'.$row['login'].'</td>';
							echo '<td>'.'<div class="championImage" style="background-image: url('.$row['skin'].')"></div>'.'</td>';
							echo '<td>'.$row['level'].'</td>';
							echo '<td class="showMap" data-map="'.$row['mapImg'].'" data-xPos="'.$row['xPos'].'" data-yPos="'.$row['yPos'].'">'.$row['map'].'('.$row['xPos'].', '.$row['yPos'].')'.'</td>';
							echo '<td>'.$row['created'].'</td>';
							echo '<td>'.$row['lastOnline'].'</td>';
							echo '<td>';
							echo '<a href="?admin&site=champions&edit='.$row['id'].'"/>Edytuj</a> '; 
							echo '<a href="?admin&site=champions&deleteChampion='.$row['id'].(isset($showUserChampions) ? "&redirect=".$edit : "").'"/>Usuń</a>';
							echo '</td>';
							echo '</tr>';
						}	
					}else{
						echo '<tr><td colspan="7">Brak postaci</td></tr>';
					}
				}else{
					die($sql_conn->error);
			}
		?>
	</tbody>
</table>
<div style="text-align:center">
	<?php
		if(!isset($showUserChampions)){
			$sql = "SELECT COUNT(*) as count FROM champions";
			if(isset($_GET['search'])){
				$name = $_GET['search'];
				$sql .= " WHERE name LIKE '%$name%'";
			}
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->fetch_assoc()['count'];
				$pageCount = ceil($count/$usersPerPage);
				for($i = 0; $i < $pageCount; $i++){
					echo '<a href="?admin&site=champions&page='.$i.(isset($_GET['search'])? "&search=".$_GET['search']:"").'">['.($i+1).']</a> ';
				}
			
			}else{
				die($sql_conn->error);
			}
		}
	?>
</div>