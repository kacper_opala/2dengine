<?php
	if(!isset($_SESSION)){
		session_start();
	}
	if(!isset($_SESSION['login'])){
		die('Sesja wygasła!');
	}
	$basedir = "../../img/";
	if(isset($_POST['getFiles'])){
		$path = $_POST['getFiles'];
		$fullpath = $basedir.$path;
		
		$files = array_values(array_diff(scandir($fullpath), array('..', '.')));
		$filesInfo_files = [];
		$filesInfo_dir = [];
		foreach($files as $file){
			if(is_dir($fullpath.'/'.$file)){
				$filesInfo_dir[] = array(
					"name"=>$file,
					"type"=> filetype($fullpath.'/'.$file)
				);
			}else{
				$filesInfo_files[] = array(
					"name"=>$file,
					"type"=> filetype($fullpath.'/'.$file)
				);
			}
		}
		$filesInfo = array_merge($filesInfo_dir,$filesInfo_files);
		echo json_encode($filesInfo);
		exit(0);
	}else if(isset($_POST['moveFile'])){

		$file = $_POST['moveFile'];
		$from = $_POST['from'];
		$to = $_POST['to'];
		$fullpath = $basedir.$to.$file;
		if(!file_exists($fullpath))
			rename($basedir.$from.$file, $fullpath);
		exit(0);
	}else if(isset($_POST['uploadFile']) && isset($_FILES) && !empty($_FILES)){
		foreach($_FILES as $file)
		{
			if ($file["error"] == UPLOAD_ERR_OK)
			{
				$path = $basedir.$_POST['uploadFile'].$file["name"];
				move_uploaded_file( $file["tmp_name"], $path);
			}
		}
		exit(0);
	}/*
	if(count($_POST)>0){
		
	}*/
?>
<style>
.file{
	width: 96px;
	height: 64px;
	background-repeat: no-repeat;
	background-position: center;
	background-size: contain;
	display:inline-block;
	margin: 10px 10px 60px 10px;
	user-select: none;
	cursor: pointer;
	position:relative;
}
.file:hover span{
	color: red;
}
.file span{
	word-wrap: break-word;
	position: absolute;
    top: 64px;
    width: 100%;
    text-align: center;
}
.files{
	overflow-y:auto;
	flex: 1 1 auto;
}
.fileDrop{
	box-sizing: border-box;
	background-color: rgb(200,200,200);
	outline: dashed 4px rgb(128, 128, 128);
}
.fileDrop::after{
	background: white;
	padding: 5px 20px;
	border-radius: 1.5em;
	content: "Upuść plik tutaj!";
	font-size: 48px;
	font-weight: bold;
	color: rgb(128, 128, 128); 
	position: absolute;
	top: 50%; 
	left: 50%; 
	transform: translate(-50%, -50%);
}
</style>
<script type="module">
	import { tooltip } from '../../js/modules/gui.js'; 
$(function() {
	const globalPath ={
		_value: '',
		basePath: 'img/',
		set Set(val){
			if(val == '/')
				this._value = '';
			else
				this._value = val;
			$('#path').val('/'+this._value);
			
		},
		get Get(){
			return this._value;
		}
	}
	$('input').droppable({
		accept: '.file',
		tolerance: "pointer",
		drop: function(event, ui) {
			const path = globalPath.basePath+globalPath.Get;
			const name = ui.draggable.data('name');
			$(this).val(path+name).trigger('change');
		}
	});
	$('#goUp').droppable({
		accept: '.file',
		tolerance: "pointer",
		drop: function(event, ui) {
			const re = /[^/]+?$/;
			const subtractDir = re.exec(globalPath.Get.slice(0,-1));
			const subtractDirLength = subtractDir ? subtractDir[0].length+1 : 0;
			const path = globalPath.Get.slice(0, -subtractDirLength);
			const name = ui.draggable.data('name');
			console.log(path, globalPath.Get);
			$.post( "php/admin/filemanager.php", {moveFile: name, from: globalPath.Get, to: path+'/'}, function() {
				$('#goRefresh').click();
			});
		}
	});
	$('#goUp').click(()=>{
		const re = /[^/]+?$/;
		const subtractDir = re.exec(globalPath.Get.slice(0,-1));
		const subtractDirLength = subtractDir ? subtractDir[0].length+1 : 0;
		const path = globalPath.Get.slice(0, -subtractDirLength);
		loadFiles(path);
	});
	$('#goRefresh').click(()=>{
		const re = /[^/]+?$/;
		loadFiles(globalPath.Get);
	});
	$(document).on('dragover', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$('.files').addClass("fileDrop");
	});
	$(document).on('dragenter', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});
	$(document).on('dragleave', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$('.files').removeClass("fileDrop");
	});
	$(".files").on('drop', (e)=>{
		$('.files').removeClass("fileDrop");
		if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
			e.preventDefault();
			e.stopPropagation();
			/*UPLOAD FILES HERE*/
			//console.log(e.originalEvent.dataTransfer.files)
			const formdata = new FormData();
			console.log('Upath',path);
			formdata.append("uploadFile", globalPath.Get);
			for(let i=0; i < e.originalEvent.dataTransfer.files.length; i++)
			{
				formdata.append("file"+i, e.originalEvent.dataTransfer.files[i]);
			}
			$.ajax({
				url: "php/admin/filemanager.php",
				type: "POST",
				data: formdata,
				processData: false,
				contentType: false,
				//dataType: "json",
				success: ()=>{
					$('#goRefresh').click();
				}
			});
		}
	});
	function loadFiles(path=''){
		$.post( 'php/admin/filemanager.php', {getFiles: path}, function( data ) {
			globalPath.Set = path;
			$('.files').html("");
			if(data.length == 0){
				$('.files').html("<p style='text-align:center; color: grey'>Katalog jest pusty</p>");
			}
			data.forEach((file)=>{
				let fileType;
				let isImage = false;
				if(file.type == 'dir')
					fileType="img/filemanagerImages/folder.png";
				else if(file.type == 'file'){
					const re = /(?:\.([^.]+))?$/;
					const ext = re.exec(file.name)[1];
					switch (ext) {
						case 'tif':
						case 'tiff':
						case 'bmp':
						case 'jpg':
						case 'jpeg':
						case 'jfif':
						case 'gif':
						case 'png':
							fileType="img/filemanagerImages/image-file.png";
							isImage = true;
							break;
						case 'json':
						case 'js':
						case 'html':
						case 'php':
						case 'css':
							fileType="img/filemanagerImages/code-file.png";
							break;
						default:
							fileType="img/filemanagerImages/file.png";
							break;
					}
				}else{
					fileType="img/filemanagerImages/file.png";
				}
				$('.files').append(`<div class="file" style="background-image: url(${fileType})" ${isImage? 'data-isimage="true"':''} data-type="${file.type}" data-name="${file.name}"><span>${file.name}</span></div>`);
			});
			$('.file').droppable({
				accept: '.file',
				tolerance: "pointer",
				drop: function(event, ui) {
					if($(this).data('type')=='dir'){
						const path = (globalPath.Get.substring(1).length == 0 ? '' :globalPath.Get);
						const name = ui.draggable.data('name');
						const dir = $(this).data('name');
						console.log(path+'/'+dir,path+'/'+name);
						$.post( "php/admin/filemanager.php", {moveFile: name, from: path+'/', to: path+'/'+dir+'/'}, function() {
							$('#goRefresh').click();
						});
					}
				}
			});
			$('.file').draggable({
				opacity: 0.4,
				revertDuration: 0,
				helper: 'clone',
				appendTo: "body",
				zIndex: 999
			});
			$('.file').dblclick(function(){
				const type= $(this).data('type');
				const isImage = $(this).data('isimage');
				
				const path2 = (globalPath.Get);//.length == 0 ? '' :globalPath.Get);
				const name = $(this).data('name');
				if(type == 'dir'){
					const path = (globalPath.Get);//.length == 0 ? '' :globalPath.Get);
					loadFiles(path+name+'/');
				}else if(type=='file' && isImage == true){
					const path = globalPath.basePath+globalPath.Get;
					const window = new MessageBox(`<img style="max-width:60vw;max-height:60vh;width: auto;height: auto; display: block; margin: 0 auto" src="${path}/${name}" />`,name, 'button2');
					window.center();
				}
			});
			$('.file').mousemove(function () {
				const type= $(this).data('type');
				const isImage = $(this).data('isimage');
				
				const path2 = (globalPath.Get);//.length == 0 ? '' :globalPath.Get);
				const name = $(this).data('name');
				if(type=='file' && isImage == true){
					const path = globalPath.basePath+globalPath.Get;
					tooltip(`<img style="max-width:320px;max-height:240px;width: auto;height: auto; display: block; margin: 0 auto"  src="${path}/${name}" />`);
				}
			});
			$('.file').mouseleave(function(){
				tooltip(null);
			});
		}, "json");
	}
	loadFiles();
});
</script>
<div style="flex: 0 1 auto;">
	<button class="button2" id="goUp">↑</button>
	<button class="button2" id="goRefresh">🗘</button>
	<input type="text" id="path" readonly/>
</div>
<div class="files">
</div>
