<?php
if(isset($_GET['new'])){
    $sql = "INSERT INTO `maps` (`id`, `name`, `background`, `battleBackground`, `layer1`, `layer2`, `weather`, `collisions`, `size`) VALUES (NULL, 'Nowa mapa', NULL, NULL, '', NULL, NULL, json_array(), Point(0,0))";
    if($rezultat=$sql_conn->query($sql)){
        $id = $sql_conn->insert_id;
        header("Location: ?admin&site=maps&edit=$id");
    }else{
        die($sql_conn->error);
    }
}
if(isset($_GET['delete'])){
    $delete = $_GET['delete'];
    $sql = "DELETE FROM maps WHERE id=$delete";
    if($rezultat=$sql_conn->query($sql)){
        header('Location: ?admin&site=maps');
    }else{
        die($sql_conn->error);
    }
}else if(isset($_GET['edit'])){
    include('php/admin/maps_edit.php');
}else{
    include('php/admin/maps_get.php');
}
?>