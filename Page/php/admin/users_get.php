<?php
	if(isset($_GET['deleteUser']) && $_SESSION['login']['access'] == 'admin' && $_SESSION['login']['id'] != $_GET['deleteUser']){
		$delete = $_GET['deleteUser'];
		$sql = "DELETE FROM users WHERE id=$delete";
		if($rezultat=$sql_conn->query($sql)){
			header('Location: ?admin&site=users');
		}else{
			die($sql_conn->error);
		}
	}
?>
<div style="text-align:right">
	<form action="">
		<input type="hidden" name="admin"/>
		<input type="hidden" name="site" value="users"/>
		<?= isset($_GET['page'])? '<input type="hidden" name="page" value="'.$_GET['page'].'"/>' : ''?>
		<input type="text" name="search" placeholder="Szukaj" autocomplete="off" <?= isset($_GET['search'])? 'value="'.$_GET['search'].'"':'' ?>/>
	</form>
</div>
<hr/>
<table class="table">
	<thead>
		<tr>
			<th>L.p</th>
			<th>Login</th>
			<th>@-mail</th>
			<th>Data utworzenia</th>
			<th>Poziom dostępu</th>
			<th>Modyfikuj</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$page = isset($_GET['page']) ? $_GET['page'] : 0;
		$usersPerPage = 10;
		$page *= $usersPerPage;
		if(isset($_GET['search'])){
			$login = $_GET['search'];
			$sql = "SELECT id, login, mail, created, access FROM users WHERE login LIKE '%$login%' ORDER BY id ASC LIMIT $usersPerPage OFFSET $page";
		}
		else
			$sql = "SELECT id, login, mail, created, access FROM users ORDER BY id ASC LIMIT $usersPerPage OFFSET $page";
		if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1;
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
						echo '<td>'.$row['login'].'</td>';
						echo '<td>'.$row['mail'].'</td>';
						echo '<td>'.$row['created'].'</td>';
						echo '<td>'.$row['access'].'</td>';
						echo '<td>';
						echo '<a href="?admin&site=users&edit='.$row['id'].'"/>Edytuj</a> ';
						if($_SESSION['login']['access'] == 'admin' && $row['id'] != $_SESSION['login']['id']) 
							echo '<a href="?admin&site=users&deleteUser='.$row['id'].'"/>Usuń</a>';
						echo '</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="7">Brak użytkowników</td></tr>';
				}
			}else{
				die($sql_conn->error);
		}
	?>
	</tbody>
</table>
<div style="text-align:center">
	<?php 
		$sql = "SELECT COUNT(*) as count FROM users";
		if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->fetch_assoc()['count'];
			$pageCount = ceil($count/$usersPerPage);
			for($i = 0; $i < $pageCount; $i++){
				echo '<a href="?admin&site=users&page='.$i.'">['.($i+1).']</a> ';
			}
			
		}else{
			die($sql_conn->error);
		}
	?>
</div>