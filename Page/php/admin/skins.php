<?php
	if(isset($_POST['addSkin'])){
        $image = $_POST['image'];
        $size = explode(',', $_POST['size']);
		$size = 'POINT('.$size[0].','.$size[1].')';
		if(isset($_GET['edit'])){
			$id = $_GET['edit'];
			$sql = "UPDATE skins SET image='$image', size=$size WHERE id=$id";
			if($rezultat=$sql_conn->query($sql)){
				header('Location: ?admin&site=skins');
			}else{
				die($sql_conn->error);
			}
		}else{
            $sql = "INSERT INTO skins (image, size) VALUES('$image', $size)";
			if(!$rezultat=$sql_conn->query($sql)){
				die($sql_conn->error);
			}
		}
	}else if(isset($_GET['delete'])){
        $delete = $_GET['delete'];
        if($delete == 1){
            die("Can't delet skin id 1");
        }
		$sql = "UPDATE champions SET skin=1 WHERE skin=$delete;
        UPDATE npcs SET skin=1 WHERE skin=$delete;
        DELETE FROM skins WHERE id=$delete";
		if($rezultat=$sql_conn->query($sql)){
			header('Location: ?admin&site=skins');
		}else{
			die($sql_conn->error);
		}
	}else if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$sql = "SELECT image, ST_X(size) as sizeX, ST_Y(size) as sizeY FROM skins WHERE id=$id";
		if($rezultat=$sql_conn->query($sql)){
			$row = $rezultat->fetch_assoc();
			$editSkins = array(
				'image' => $row['image'],
				'sizeX' => $row['sizeX'],
				'sizeY' => $row['sizeY']
			);
		}else{
			die($sql_conn->error);
		}
	}
?>
<style>
.addSkins, manageNews{
	width: fit-content;
	margin: 0 auto;
}
.manageSkins{
	width: 80%;
	margin: 0 auto;
}
</style>
<script>
$(function() {
    $('#imageUrl').change(function(){
		$.get($('#imageUrl').val()).done(function() { 
            $('#imagePreview').css({"background-image": `url('${$('#imageUrl').val()}')`});
            const img = new Image();
            img.onload = function() {
                $('#skinSize').val(`${img.width},${img.height}`)
            }
            img.src = $('#imageUrl').val();
		});
	});
});
</script>
<div class="addSkins">
	<form action="" method="POST">
		<table>
			<tr>
				<td><input type="text" name="image" placeholder="url Obrazka" autocomplete="off" id="imageUrl" <?= isset($editSkins)? 'value="'.$editSkins['image'].'"' : '' ?> /></td>
				<td><input type="text" name="size" placeholder="Rozmiar skinu" autocomplete="off" id="skinSize" <?= isset($editSkins)? 'value="'.$editSkins['sizeX'].','.$editSkins['sizeY'].'"' : '' ?> /></td>
                <td rowspan="2">
                    <div id="imagePreview" class="championImage" style="background-image: url('<?= isset($editSkins)? $editSkins['image'] : '' ?>')"></div>
                </td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:right">
					<button type="submit" name="addSkin" class="button2"><?= isset($editSkins)? 'Edytuj' : 'Dodaj' ?></button>
				</td>
                <td></td>
			</tr>
		</table>
	</form>
</div>
<hr/>
<div class="manageSkins">
	<table class="table">
		<thead>
			<tr>
				<th>L.p</th>
				<th>Obrazek</th>
				<th>Rozmiar</th>
				<th>Modyfikuj</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$skinsPerPage = 10;
			$page *= $skinsPerPage;
			$sql = "SELECT id, image, ST_X(size) as sizeX, ST_Y(size) as sizeY FROM skins ORDER BY id ASC LIMIT $skinsPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1*($page+1);
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
						echo '<td>'.'<div class="championImage" style="background-image: url('.$row['image'].')"></div>'.'</td>';
						echo '<td>'.$row['sizeX'].','.$row['sizeY'].'</td>';
						echo '<td>';
    					echo '<a href="?admin&site=skins&edit='.$row['id'].'"/>Edytuj</a> ';
                        if($row['id'] != 1) echo '<a href="?admin&site=skins&delete='.$row['id'].'"/>Usuń</a>';
                        echo '</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="4">Brak skinów</td></tr>';
				}
			}else{
				die($sql_conn->error);
			}
		?>
		</tbody>
	</table>
	<div style="text-align:center">
		<?php 
			$sql = "SELECT COUNT(*) as count FROM skins";
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->fetch_assoc()['count'];
				$pageCount = ceil($count/$skinsPerPage);
				for($i = 0; $i < $pageCount; $i++){
					echo '<a href="?admin&site=skins&page='.$i.'">['.($i+1).']</a> ';
				}
			}else{
				die($sql_conn->error);
			}
		?>
	</div>
</div>