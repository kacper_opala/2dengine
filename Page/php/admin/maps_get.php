<style>
.manageMaps{
	width: 80%;
	margin: 0 auto;
}
</style>
<script>
$(function() {
});
</script>
<a href="?admin&site=maps&new" class="button2">Nowa mapa</a>
<div class="manageMaps">
	<table class="table">
		<thead>
			<tr>
				<th>L.p</th>
				<th>Nazwa</th>
				<th>Rozmiar</th>
				<th>Modyfikuj</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$mapsPerPage = 10;
			$page *= $mapsPerPage;
			$sql = "SELECT id, name, layer1, ST_X(size) as sizeX, ST_Y(size) as sizeY FROM maps LIMIT $mapsPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1*($page+1);
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
                        echo '<td class="showMap" data-map="'.$row['layer1'].'" data-xPos="'.$row['sizeX'].'" data-yPos="'.$row['sizeY'].'">'.$row['name'].'</td>';
                        echo '<td>'.$row['sizeX'].', '.$row['sizeY'].'</td>';
						echo '<td>
							<a href="?admin&site=maps&edit='.$row['id'].'"/>Edytuj</a>
							<a href="?admin&site=maps&delete='.$row['id'].'"/>Usuń</a>
							</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="4">Brak map</td></tr>';
				}
			}else{
				die($sql_conn->error);
			}
		?>
		</tbody>
	</table>
	<div style="text-align:center">
		<?php 
			$sql = "SELECT COUNT(*) as count FROM maps";
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->fetch_assoc()['count'];
				$pageCount = ceil($count/$mapsPerPage);
				for($i = 0; $i < $pageCount; $i++){
					echo '<a href="?admin&site=maps&page='.$i.'">['.($i+1).']</a> ';
				}
			}else{
				die($sql_conn->error);
			}
		?>
	</div>
</div>