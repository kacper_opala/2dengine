<?php
    if(isset($_POST['save'])){
        $id = $_GET['edit'];
        $name = $_POST['name'];
        $background = strlen($_POST["background"]) > 0 ? $_POST["background"] : 'null';
        $battleBackground = strlen($_POST["battleBackground"]) > 0 ? $_POST["battleBackground"] : 'null';
        $layer1 = $_POST['layer1'];
        $layer2 = strlen($_POST["layer2"]) > 0 ? $_POST["layer2"] : 'null';
        $weather = strlen($_POST["weather"]) > 0 ? $_POST["weather"] : 'null';
        $collisions = $_POST['collisions'];
        $sizeX = $_POST['sizeX'];
        $sizeY = $_POST['sizeY'];

        $sql = "UPDATE maps SET name='$name', background='$background', battleBackground='$battleBackground', layer1='$layer1', layer2='$layer2', weather='$weather', collisions='$collisions', size=Point($sizeX,$sizeY) WHERE id=$id";
		if(!$rezultat=$sql_conn->query($sql)){
            die($sql_conn->error);
		}
    }
    if(isset($_GET['edit'])){
        $id = $_GET['edit'];
		$sql = "SELECT m.id, m.name, m.background, m.battleBackground, m.layer1, m.layer2, m.weather, m.collisions, ST_X(m.size) as sizeX, ST_Y(m.size) as sizeY FROM maps as m WHERE id=$id";
		if($rezultat=$sql_conn->query($sql)){
            $row = $rezultat->fetch_assoc();
            $mapId = $row['id'];
			$editMap = array(
				'name' => $row['name'],
				'background' => $row['background'],
				'battleBackground' => $row['battleBackground'],
				'layer1' => $row['layer1'],
				'layer2' => $row['layer2'],
				'weather' => $row['weather'],
				'collisions' => $row['collisions'],
				'sizeX' => $row['sizeX'],
				'sizeY' => $row['sizeY'],
			);
		}else{
			die($sql_conn->error);
        }
        $sql = "SELECT 
            s.image as skin, 
            ST_X(s.size) as skinW, 
            ST_Y(s.size) as skinH, 
            ST_X(mn.position) as posX, 
            ST_Y(mn.position) as posY 
        FROM mapnpc as mn 
        JOIN npcs as n ON mn.npcId=n.id 
        JOIN skins as s ON s.id=n.skin
        WHERE mn.mapId=$mapId";
		if($rezultat=$sql_conn->query($sql)){            
            echo '<script> const npcList = [';
            while($row = $rezultat->fetch_assoc()){
                echo json_encode($row).',';
            }
            echo ']</script>';
		}else{
			die($sql_conn->error);
		}
    }
?>
<style>
    #renderer {
        margin: 0;
        image-rendering: pixelated;
}
</style>
<script src="js/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
<link href="js/msdropdown/css/dd.css" rel="stylesheet" type="text/css" />
<form method="POST" action="">
    <input type="text" class="smallInput" name="name"  placeholder="Nazwa" <?= isset($editMap)? 'value="'.$editMap['name'].'"' : '' ?>/>
    <input type="text" class="smallInput" name="background" placeholder="Tło" <?= isset($editMap)? 'value="'.$editMap['background'].'"' : '' ?>/>
    <input type="text" class="smallInput" name="battleBackground" placeholder="Tło Walki" <?= isset($editMap)? 'value="'.$editMap['battleBackground'].'"' : '' ?>/>
    <input type="text" class="smallInput" name="layer1" id="imageUrl" placeholder="Dolna Warstwa" <?= isset($editMap)? 'value="'.$editMap['layer1'].'"' : '' ?>/>
    <input type="text" class="smallInput" name="layer2" placeholder="Górna Warstwa" <?= isset($editMap)? 'value="'.$editMap['layer2'].'"' : '' ?>/>
    <input type="text" class="smallInput" name="weather" placeholder="Pogoda" <?= isset($editMap)? 'value="'.$editMap['weather'].'"' : '' ?>/>
    <input type="hidden" name="collisions" id="collisions" <?= isset($editMap)? 'value="'.$editMap['collisions'].'"' : '' ?>/>
    <input type="hidden" name="sizeX" id="sizeX" <?= isset($editMap)? 'value="'.$editMap['sizeX'].'"' : '' ?>/>
    <input type="hidden" name="sizeY" id="sizeY" <?= isset($editMap)? 'value="'.$editMap['sizeY'].'"' : '' ?>/>
    <button type="submit" name="save" class="button2">Zapisz</button>
</form>
<button class="button2" id="mode_collisions">Kolizje</button>
<button class="button2" id="mode_npc">NPC</button>
<select name="skin" id="skin" placeholder="Skin" >
<?php
    $sql = "SELECT n.id, s.image FROM npcs as n JOIN skins as s ON s.id=n.skin ORDER BY n.id ASC";
    if($rezultat=$sql_conn->query($sql)){
        while($row = $rezultat->fetch_assoc()){
            $selected = isset($npcData) && $npcData['skin'] == $row['id']? "selected":"";
            echo "<option {$selected} value='{$row['id']}' data-image='{$row['image']}'></option>";
        }
    }else{
        die($sql_conn->error);
    }
?>
</select>
<script type="module">
    import {RenderEngine} from './js/modules/renderer.js'
    $(function() {
        $( "#skin" ).msDropDown({visibleRows:2});

        const renderer = new RenderEngine.Init('renderer', 60, {width:320, height:240});
        const camera = new RenderEngine.Camera(renderer);
        let collisions = JSON.parse($('#collisions').val());
        let cursor = null;
        let mode = "collisions";
        $('#mode_collisions').click(()=>{ mode = 'collisions'; });
        $('#mode_npc').click(()=>{ mode = 'npc'; });
        renderer.onClick((entity, mouse) => {
            const pos = {x: Math.floor(mouse.x/32), y: Math.floor(mouse.y/32)};
            //console.log(mouse, pos);
            switch(mode){
                case 'collisions':
                    collisions[pos.y][pos.x] = collisions[pos.y][pos.x] ? 0 : 1;
                    $('#collisions').val(JSON.stringify(collisions));
                    break;
                case 'npc':
                    //add npc
                    break;
            }
        });
        renderer.onHover((entity, mouse) => {
            const pos = {x: Math.floor(mouse.x/32), y: Math.floor(mouse.y/32)};
            $('#cursorPos').html(`${pos.x},${pos.y}`);
            if(cursor){
                cursor.x = pos.x*32;
                cursor.y = pos.y*32;
            }
        });
        renderer.pollEvent = (delta)=>{
            renderer.ctx.fillStyle = 'rgba(255, 0, 0, 0.3)';
            try{
                for (let i = 0; i < collisions.length; i++) {
                    for (let j = 0; j < collisions[i].length; j++) {
                        if(collisions[i][j])
                            renderer.ctx.fillRect(j*32, i*32, 32, 32);
                    }
                }
            }catch(e){

            }
        }
        function loadMap(url){
            RenderEngine.RemoveAllEntity(renderer);
            cursor = new RenderEngine.Entity(
					renderer,
					2,
					'cursor',
					{ x: 0, y: 0 },
					'img/mapeditor_cursor.png',
					{ width: 32, height: 32 },
					{ width: 32, height: 32 },
					{ x: 0, y: 0 },
					null
                );
            console.log(cursor);
            const img = new Image();
            img.onload = ()=>{
                const size = {width: Math.ceil(img.width/32), height: Math.ceil(img.height/32)};
                if(collisions.length == size.height && collisions[1].length == size.width){

                }else{
                    collisions = new Array(size.height);
                    for (let i = 0; i < collisions.length; i++) {
                        collisions[i] = new Array(size.width);
                        for (let j = 0; j < collisions[i].length; j++) {
                            collisions[i][j] = 0;
                        }
                    }
                }
                window.collisions = collisions;
                $('#sizeX').val(size.width);
                $('#sizeY').val(size.height);
                camera.Resize({width: img.width, height: img.height});
                new RenderEngine.Entity(
					renderer,
					0,
					'layer0',
					{ x: 0, y: 0 },
					url,
					{ width: img.width, height: img.height },
					{ width: img.width, height: img.height },
					{ x: 0, y: 0 },
					null
                );
            };
            img.src = url;
            npcList.forEach(n=>{
                const npc = new RenderEngine.Entity(
					renderer,
					1,
					'npc',
					{ x: n.posX*32, y: n.posY*32 },
					n.skin,
					{ width: n.skinW/4, height: n.skinH/4 },
					{ width: n.skinW/4, height: n.skinH/4 },
					{ x: 0, y: 0 },
					null
                );
                npc.orginX = n.skinW / 8 - 16;
				npc.orginY = n.skinH / 4 - 32;
            });
        }
        $('#imageUrl').change(function(){
            loadMap($('#imageUrl').val());
        });
        if($('#imageUrl').val() != ""){
            loadMap($('#imageUrl').val());
        }
        
        window.camera = camera;
        window.renderer = renderer;
        window.collisions = collisions;
    });
</script>
<div style="max-width: 100%; display: block; overflow: auto; max-height:90%">
<canvas id="renderer"></canvas>
</div>
<span id="cursorPos"></span>