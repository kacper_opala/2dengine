<?php
	if(isset($_POST['addNews'])){
		$title = $_POST['title'];
		$image = $_POST['image'];
		$text = $_POST['text'];
		$author = $_SESSION['login']['id'];
		if(isset($_GET['edit'])){
			$id = $_GET['edit'];
			$sql = "UPDATE news SET title='$title', image='$image', text='$text', author=$author WHERE id=$id";
			if($rezultat=$sql_conn->query($sql)){
				header('Location: ?admin&site=news');
			}else{
				die($sql_conn->error);
			}
		}else{
			$sql = "INSERT INTO news (title, image, text, author) VALUES('$title', '$image', '$text', $author)";
			if(!$rezultat=$sql_conn->query($sql)){
				die($sql_conn->error);
			}
		}
	}else if(isset($_GET['delete'])){
		$delete = $_GET['delete'];
		$sql = "DELETE FROM news WHERE id=$delete";
		if($rezultat=$sql_conn->query($sql)){
			header('Location: ?admin&site=news');
		}else{
			die($sql_conn->error);
		}
	}else if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$sql = "SELECT title, image, text FROM news WHERE id=$id";
		if($rezultat=$sql_conn->query($sql)){
			$row = $rezultat->fetch_assoc();
			$editNews = array(
				'title' => $row['title'],
				'image' => $row['image'],
				'text' => $row['text']
			);
		}else{
			die($sql_conn->error);
		}
	}
?>
<style>
.addNews{
	width: fit-content;
	margin: 0 auto;
}
.manageNews{
	width: 80%;
	margin: 0 auto;
}
</style>
<script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
<script>
$(function() {
	$('#imageUrl').change(function(){
		$.get($('#imageUrl').val()).done(function() { 
			$('#imagePreview').attr("src",$('#imageUrl').val());
		});
	});
	ClassicEditor
        .create( document.querySelector( '#editor' ) ,{
			toolbar: [ 'undo', 'redo', '|',/* 'fontsize', 'fontColor', '|',*/ 'Heading', '|', 'bold', 'italic', 'underline', '|', 'link', 'bulletedList', 'numberedList', '|', 'blockQuote', 'insertTable', 'mediaEmbed', '|', 'Source']
		})
        .catch( error => {
            console.error( error );
        } );
});
</script>
<div class="addNews">
	<form action="" method="POST">
		<table>
			<tr>
				<td><input type="text" name="title" placeholder="Tytuł" autocomplete="off" <?= isset($editNews)? 'value="'.$editNews['title'].'"' : '' ?>/></td>
				<td><input type="text" name="image" placeholder="url Obrazka" autocomplete="off" id="imageUrl" <?= isset($editNews)? 'value="'.$editNews['image'].'"' : '' ?> /><br/></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					<textarea id="editor" name="text" placeholder="Treść" ><?= isset($editNews)? $editNews['text'] : '' ?></textarea>
				</td>
				<td>
					<img id="imagePreview" style="width:auto; height: auto; max-width: 150px; max-height: 150px" <?= isset($editNews)? 'src="'.$editNews['image'].'"' : '' ?>/>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:right">
					<button type="submit" name="addNews" class="button2"><?= isset($editNews)? 'Edytuj' : 'Dodaj' ?></button>
				</td>
				<td></td>
			</tr>
		</table>
	</form>
</div>
<hr/>
<div class="manageNews">
	<table class="table">
		<thead>
			<tr>
				<th>L.p</th>
				<th>Tytuł</th>
				<th>Obrazek</th>
				<th>Treść</th>
				<th>Autor</th>
				<th>Data</th>
				<th>Modyfikuj</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$newsPerPage = 10;
			$page *= $newsPerPage;
			$sql = "SELECT n.id, n.title, n.image, n.text, n.date, u.login  FROM news as n JOIN users as u ON n.author = u.id ORDER BY n.id DESC LIMIT $newsPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1*($page+1);
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
						echo '<td>'.$row['title'].'</td>';
						echo '<td><img width="64" src="'.$row['image'].'"/></td>';
						$text = strip_tags($row['text']);
						echo '<td style="text-align:left">'.substr($text,0,40).(strlen($text)>40? '...':'').'</td>';
						echo '<td>'.$row['login'].'</td>';
						echo '<td>'.$row['date'].'</td>';
						echo '<td>
							<a href="?admin&site=news&edit='.$row['id'].'"/>Edytuj</a>
							<a href="?admin&site=news&delete='.$row['id'].'"/>Usuń</a>
							</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="7">Brak newsów</td></tr>';
				}
			}else{
				die($sql_conn->error);
			}
		?>
		</tbody>
	</table>
	<div style="text-align:center">
		<?php 
			$sql = "SELECT COUNT(*) as count FROM news";
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->fetch_assoc()['count'];
				$pageCount = ceil($count/$newsPerPage);
				for($i = 0; $i < $pageCount; $i++){
					echo '<a href="?admin&site=news&page='.$i.'">['.($i+1).']</a> ';
				}
			}else{
				die($sql_conn->error);
			}
		?>
	</div>
</div>