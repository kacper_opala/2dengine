<?php
    if(isset($_POST["addNpc"])){
        $name= $_POST["name"];
        $level= strlen($_POST["level"]) > 0 ? $_POST["level"] : 'null';
        $health= strlen($_POST["health"]) > 0 ? $_POST["health"] : 'null';
        $attackPower= strlen($_POST["attackPower"]) > 0 ? $_POST["attackPower"] : 'null';
        $skin= $_POST["skin"];
        $type= $_POST["type"];
        $respawnTime= strlen($_POST["respawnTime"]) > 0 ? $_POST["respawnTime"] : 'null';
        $action= $_POST["action"];
        if(isset($_GET['edit'])){
            $id = $_GET['edit'];
            $sql = "UPDATE npcs SET `name`='$name',`level`=$level,`health`=$health,`attackPower`=$attackPower,`skin`=$skin,`type`='$type',`respawnTime`=$respawnTime,`action`=$action where id=$id";
            if($rezultat=$sql_conn->query($sql)){
				header('Location: ?admin&site=npcs');
			}else{
				die($sql_conn->error);
			}
        }else{
            $sql = "INSERT INTO npcs (`id`, `name`, `level`, `health`, `attackPower`, `skin`, `type`, `respawnTime`, `action`) VALUES (NULL, '$name', $level, $health, $attackPower, $skin, '$type', $respawnTime, $action)";
            if(!$rezultat=$sql_conn->query($sql)){
                die($sql_conn->error);
            }
        }
    }else if(isset($_GET['delete'])){
		$delete = $_GET['delete'];
		$sql = "DELETE FROM npcs WHERE id=$delete";
		if($rezultat=$sql_conn->query($sql)){
			header('Location: ?admin&site=npcs');
		}else{
			die($sql_conn->error);
		}
	}else if(isset($_GET["edit"])){
        $id= $_GET["edit"];
        $sql = "SELECT * FROM npcs where id=$id";
        if($rezultat=$sql_conn->query($sql)){
            $npcData = $rezultat->fetch_assoc();
        }else{
            die($sql_conn->error);
        }
    }
?>
<script src="js/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
<link href="js/msdropdown/css/dd.css" rel="stylesheet" type="text/css" />
<script>
    $(function() {
        $( "#skin" ).msDropDown({visibleRows:1.5});
    });
</script>
<style>
    .addNpc{
        width: fit-content;
        margin: 0 auto;
        text-align:center;
    }
</style>
<div class="addNpc">
    <form method="POST" action="">
        <table>
            <tr>
                <td><input type="text" name="name" placeholder="Nazwa" <?=isset($npcData)? "value=\"{$npcData['name']}\"":"" ?> /></td>
                <td rowspan="7">
                    <select name="skin" id="skin" placeholder="Skin" >
                        <?php
                            $sql = "SELECT id, image FROM skins";
                            if($rezultat=$sql_conn->query($sql)){
                                while($row = $rezultat->fetch_assoc()){
                                    $selected = isset($npcData) && $npcData['skin'] == $row['id']? "selected":"";
                                    echo "<option {$selected} value='{$row['id']}' data-image='{$row['image']}'></option>";
                                }
                            }else{
                                die($sql_conn->error);
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="number" name="level" placeholder="Poziom" <?=isset($npcData)? "value=\"{$npcData['level']}\"":"" ?> /></td>
            </tr>
            <tr>
                <td><input type="number" name="health" placeholder="Poziom zdrowia" <?=isset($npcData)? "value=\"{$npcData['health']}\"":"" ?> /></td>
            </tr>
            <tr>
                <td><input type="number" name="attackPower" placeholder="Siła ataku" <?=isset($npcData)? "value=\"{$npcData['attackPower']}\"":"" ?> /></td>
            </tr>
            <tr>
                <td>
                    <select name="type" placeholder="Typ" >
                        <?php 
                            $enum = get_enum_values('npcs','type');
                            foreach($enum as $val){
                                $selected = isset($npcData) && $npcData['type'] == $val ? "selected":"";
                                echo "<option {$selected} value='$val' >$val</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="number" name="respawnTime" placeholder="Czas odrodzenia" <?=isset($npcData)? "value=\"{$npcData['respawnTime']}\"":"" ?> /></td>
            </tr>
            <tr>
                <td>
                    <select name="action" placeholder="Akcja" >
                        <option value="null">NULL</option>
                        <?php
                            $sql = "SELECT id, description FROM action";
                            if($rezultat=$sql_conn->query($sql)){
                                while($row = $rezultat->fetch_assoc()){
                                    $selected = isset($npcData) && $npcData['action'] == $row['id']? "selected":"";
                                    echo "<option {$selected} value='".$row['id']."'>".$row['description']."</option>";
                                }
                            }else{
                                die($sql_conn->error);
                            }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <button type="submit" name="addNpc" class="button2">Zapisz</button>
    </form>
</div>
<hr/>
<table class="table">
    <thead>
        <tr>
            <th>L.p</th>
            <th>Nazwa</th>
            <th>Poziom</th>
            <th>Ilość zdrowia</th>
            <th>Siła ataku</th>
            <th>Skin</th>
            <th>Typ</th>
            <th>Czas odrodzenia</th>
            <th>Opis Akcji</th>
            <th>Modyfikuj</th>
        </tr>
    </thead>
    <tbody>
        <?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$npcPerPage = 10;
			$page *= $npcPerPage;
			$sql = "SELECT n.id, n.name, n.level, n.health, n.attackPower, s.image, n.type, n.respawnTime, a.description 
            FROM npcs as n 
            LEFT JOIN skins as s ON n.skin = s.id
            LEFT JOIN action as a ON n.action = a.id
            ORDER BY n.id ASC LIMIT $npcPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1*($page+1);
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
						echo '<td>'.$row['name'].'</td>';
						echo '<td>'.$row['level'].'</td>';
						echo '<td>'.$row['health'].'</td>';
                        echo '<td>'.$row['attackPower'].'</td>';
                        echo '<td>'.'<div class="championImage" style="background-image: url('.$row['image'].')"></div>'.'</td>';
						echo '<td>'.$row['type'].'</td>';
						echo '<td>'.$row['respawnTime'].'</td>';
						echo '<td>'.$row['description'].'</td>';
						echo '<td>
							<a href="?admin&site=npcs&edit='.$row['id'].'"/>Edytuj</a>
							<a href="?admin&site=npcs&delete='.$row['id'].'"/>Usuń</a>
							</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="9">Brak NPC</td></tr>';
				}
			}else{
				die($sql_conn->error);
			}
		?>
    </tbody>
</table>
<div style="text-align:center">
    <?php 
        $sql = "SELECT COUNT(*) as count FROM npcs";
        if($rezultat=$sql_conn->query($sql)){
            $count = $rezultat->fetch_assoc()['count'];
            $pageCount = ceil($count/$npcPerPage);
            for($i = 0; $i < $pageCount; $i++){
                echo '<a href="?admin&site=npcs&page='.$i.'">['.($i+1).']</a> ';
            }
        }else{
            die($sql_conn->error);
        }
    ?>
</div>