<?php
	if(isset($_POST['addItem'])){
		$name = $_POST['name'];
		$icon = $_POST['icon'];
		$type = $_POST['type'];
		$stack = $_POST['stack'];
		$attributes = $_POST['attributes'];
		$action = $_POST['action'];
		$value = $_POST['value'];
		$description = $_POST['description'];
		
		if(isset($_GET['edit'])){
			$id = $_GET['edit'];
			$sql = "UPDATE items SET name='$name', icon='$icon', type='$type', stack=$stack, attributes='$attributes', value=$value, description='$description'  WHERE id=$id";
			if($rezultat=$sql_conn->query($sql)){
				header('Location: ?admin&site=items');
			}else{
				die($sql_conn->error);
			}
		}else{
			$sql = "INSERT INTO items (name, icon, type, stack, attributes, action, value, description) VALUES('$name', '$icon', '$type', $stack, '$attributes', $action, $value, '$description')";
			if(!$rezultat=$sql_conn->query($sql)){
				die($sql_conn->error);
			}
		}
	}else if(isset($_GET['delete'])){
		$delete = $_GET['delete'];
		$sql = "DELETE FROM items WHERE id=$delete";
		if($rezultat=$sql_conn->query($sql)){
			header('Location: ?admin&site=items');
		}else{
			die($sql_conn->error);
		}
	}else if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$sql = "SELECT * FROM items WHERE id=$id";
		if($rezultat=$sql_conn->query($sql)){
			$row = $rezultat->fetch_assoc();
			$editItem = array(
				'name' => $row['name'],
				'icon' => $row['icon'],
				'type' => $row['type'],
				'stack' => $row['stack'],
				'attributes' => $row['attributes'],
				'action' => $row['action'],
				'value' => $row['value'],
				'description' => $row['description'],
			);
		}else{
			die($sql_conn->error);
		}
	}
?>
<style>
.addItem{
	width: fit-content;
	margin: 0 auto;
}
.manageItems{
	width: 80%;
	margin: 0 auto;
}
</style>
<script>
$(function() {
	$('#imageUrl').change(function(){
		$.get($('#imageUrl').val()).done(function() { 
			$('#imagePreview').attr("src",$('#imageUrl').val());
		});
	});
});
</script>
<div class="addItem">
	<form action="" method="POST">
		<table>
			<tr>
				<td><input type="text" class="smallInput" name="name" placeholder="Nazwa" autocomplete="off" <?= isset($editItem)? 'value="'.$editItem['name'].'"' : '' ?>/></td>
				<td><input type="text" name="icon" placeholder="url Obrazka" autocomplete="off" id="imageUrl" <?= isset($editItem)? 'value="'.$editItem['icon'].'"' : '' ?> /><br/></td>
                <td>
					<img id="imagePreview" style="width:auto; height: auto; max-width: 150px; max-height: 150px" <?= isset($editItem)? 'src="'.$editItem['icon'].'"' : '' ?>/>
				</td>
                <td>
                    <select name="type" class="smallInput" placeholder="Typ" >
                        <?php 
                            $enum = get_enum_values('items','type');
                            foreach($enum as $val){
                                $selected = isset($editItem) && $editItem['type'] == $val ? "selected":"";
                                echo "<option {$selected} value='$val' >$val</option>";
                            }
                        ?>
                    </select>
                </td>
				<td><input type="number" class="smallInput" name="stack" placeholder="Stack" autocomplete="off" <?= isset($editItem)? 'value="'.$editItem['stack'].'"' : '' ?>/></td>
				<td>
                    <select name="action" placeholder="Akcja" >
                        <option value="null">NULL</option>
                        <?php
                            $sql = "SELECT id, description FROM action";
                            if($rezultat=$sql_conn->query($sql)){
                                while($row = $rezultat->fetch_assoc()){
                                    $selected = isset($editItem) && $editItem['action'] == $row['id']? "selected":"";
                                    echo "<option {$selected} value='".$row['id']."'>".$row['description']."</option>";
                                }
                            }else{
                                die($sql_conn->error);
                            }
                        ?>
                    </select>
                </td>
				<td><input type="number" class="smallInput" name="value" placeholder="Wartość" autocomplete="off" <?= isset($editItem)? 'value="'.$editItem['value'].'"' : '' ?>/></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="7">
					<textarea style="width:100%" name="attributes" placeholder="Atrybuty" ><?= isset($editItem)? $editItem['attributes'] : '' ?></textarea>
				</td>
			</tr>
            <tr>
				<td colspan="7">
					<textarea style="width:100%" name="description" placeholder="Opis" ><?= isset($editItem)? $editItem['description'] : '' ?></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="7" style="text-align:right">
					<button type="submit" name="addItem" class="button2"><?= isset($editNews)? 'Edytuj' : 'Dodaj' ?></button>
				</td>
			</tr>
		</table>
	</form>
</div>
<hr/>
<div class="manageItems">
	<table class="table">
		<thead>
			<tr>
				<th>L.p</th>
				<th>Nazwa</th>
				<th>Obrazek</th>
				<th>Typ</th>
				<th>Stack</th>
				<th>Akcja</th>
				<th>Wartość</th>
				<th>Atrybuty</th>
				<th>Opis</th>
				<th>Modyfikuj</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$page = isset($_GET['page']) ? $_GET['page'] : 0;
			$itemsPerPage = 10;
			$page *= $itemsPerPage;
			$sql = "SELECT * FROM items ORDER BY id ASC LIMIT $itemsPerPage OFFSET $page";
			if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->num_rows;
				if($count > 0){
					$i = 1*($page+1);
					while($row = $rezultat->fetch_assoc()){
						echo '<tr>';
						echo '<td>'.$i++.'</td>';
						echo '<td>'.$row['name'].'</td>';
						echo '<td><img width="64" src="'.$row['icon'].'"/></td>';
						echo '<td>'.$row['type'].'</td>';
						echo '<td>'.$row['stack'].'</td>';
						echo '<td>'.$row['action'].'</td>';
						echo '<td>'.$row['value'].'</td>';
						echo '<td>'.$row['attributes'].'</td>';
						echo '<td>'.$row['description'].'</td>';
						echo '<td>
							<a href="?admin&site=items&edit='.$row['id'].'"/>Edytuj</a>
							<a href="?admin&site=items&delete='.$row['id'].'"/>Usuń</a>
							</td>';
						echo '</tr>';
					}	
				}else{
					echo '<tr><td colspan="10">Brak przedmiotów</td></tr>';
				}
			}else{
				die($sql_conn->error);
			}
		?>
		</tbody>
	</table>
	<div style="text-align:center">
		<?php 
			$sql = "SELECT COUNT(*) as count FROM items";
			if($rezultat=$sql_conn->query($sql)){
				$count = $rezultat->fetch_assoc()['count'];
				$pageCount = ceil($count/$itemsPerPage);
				for($i = 0; $i < $pageCount; $i++){
					echo '<a href="?admin&site=news&page='.$i.'">['.($i+1).']</a> ';
				}
			}else{
				die($sql_conn->error);
			}
		?>
	</div>
</div>