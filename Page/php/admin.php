<?php
if(!$_SESSION['login']['access'] == 'moderator' && !$_SESSION['login']['access'] == 'admin'){
	die('Brak uprawnień');
}
if(!isset($_GET['site'])){
	header('Location: ?admin&site=news');
}
function isCurrentSiteActive($site){
	if(!isset($_GET['site'])){
		return '';
	}else{
		if($_GET['site'] == $site){
			return 'class="currentLink"';
		}else{
			return '';
		}
	}
}
?>
<link rel="stylesheet" href="css/admin.css" />
<script>
$(function() {
	$( ".content" ).resizable({
		handles: "s",
		resize: function( event, ui ) {
			$( ".filemanager" ).outerHeight(window.innerHeight-ui.size.height,true);
		},
		stop: function(event, ui ){
			setCookie("containerSize" ,ui.size.height, 365);
		}
	});
	$(window).resize(()=>{
		let ch = $( ".content" ).outerHeight(true);
		if(ch>window.innerHeight)
			$( ".content" ).outerHeight(window.innerHeight-96, true);
		ch = $( ".content" ).outerHeight(true);
		$( ".filemanager" ).outerHeight(window.innerHeight-ch,true);
	});
	const containerSize = getCookie("containerSize");
	if(containerSize != "" && containerSize < window.innerHeight){
		$( ".content" ).outerHeight(containerSize, true);
		$( ".filemanager" ).outerHeight(window.innerHeight-containerSize,true);
	}
});
</script>
<script type="module">
import { tooltip } from './js/modules/gui.js';
$(function () {
	$('.showMap').mousemove(function(){
		//console.log(this);
		tooltip(`<img src="${$(this).data('map')}"  style="width: auto; height: auto; max-width: 600px; max-height:480px; display:block"/>`);
	});
	$('.showMap').mouseleave(()=>{
		tooltip(null);
	});
});
</script>
<div class="menu">
MENU
<a href="?admin&site=news" <?= isCurrentSiteActive('news') ?>>Newsy</a>
<a href="?admin&site=settings" <?= isCurrentSiteActive('settings') ?>>Ustawienia</a>
<a href="?admin&site=server" <?= isCurrentSiteActive('server') ?>>Serwer</a>
<a href="?admin&site=users" <?= isCurrentSiteActive('users') ?>>Użytkownicy</a>
<a href="?admin&site=champions" <?= isCurrentSiteActive('champions') ?>>Postacie</a>
<a href="?admin&site=items" <?= isCurrentSiteActive('items') ?>>Przedmioty</a>
<a href="?admin&site=skils" <?= isCurrentSiteActive('skils') ?>>Umiejętności</a>
<a href="?admin&site=skins" <?= isCurrentSiteActive('skins') ?>>Skórki</a>
<a href="?admin&site=npcs" <?= isCurrentSiteActive('npcs') ?>>Npc</a>
<a href="?admin&site=actions" <?= isCurrentSiteActive('actions') ?>>Edytor Akcji</a>
<a href="?admin&site=maps" <?= isCurrentSiteActive('maps') ?>>Mapy</a>
<a href="?admin&site=levelstats" <?= isCurrentSiteActive('levelstats') ?>>Krzywe statystyk</a>
<a href="?admin&site=gamestats" <?= isCurrentSiteActive('gamestats') ?>>Statystyki</a>
<a href="?">Strona Główna</a>
</div>

<div class="container">
	<div class="content">
		<?php
			$site = $_GET['site'];
			switch ($site) {
				case 'news':
					include("php/admin/news.php");
					break;
				case 'settings':
					echo $site;
					break;
				case 'server':
					include("php/admin/server.php");
					break;
				case 'users':
					include("php/admin/users.php");
					break;
				case 'champions':
					include("php/admin/champions.php");
					break;
				case 'items':
					include("php/admin/items.php");
					break;
				case 'skils':
					echo $site;
					break;
				case 'skins':
					include("php/admin/skins.php");
					break;
				case 'npcs':
					include("php/admin/npc.php");
					break;
				case 'actions':
					echo $site;
					break;
				case 'maps':
					include("php/admin/maps.php");
					break;
				case 'levelstats':
					echo $site;
					break;
				case 'gamestats':
					echo $site;
					break;
				
			}
			
		?>
	</div>
	<div class="filemanager">
		<?php include("php/admin/filemanager.php"); ?>
	</div>
</div>