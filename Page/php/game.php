<?php
    $sql = "UPDATE `champions` SET `lastOnline`=CURRENT_TIMESTAMP WHERE id = ".$_POST['champion'];
    if(!$rezultat=$sql_conn->query($sql)){
        die($sql_conn->error);
    }
?>
<link rel="stylesheet" href="css/game.css" />
<script id="idLoader">const championID = <?= $_POST['champion'];?>; document.getElementById('idLoader').remove()</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.3/socket.io.min.js" integrity="sha512-uElffqRAo44n9cqo42itgRMY0L0zNZWEj3RGTQwyQsITiz4CB6rALOu1/S4O5iy/vFTMNs8gmTpDXB8mfQpjOg==" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/pathfinding-browser.min.js"></script>
<script type="module" src="js/game.js"></script>
<canvas id="renderer"></canvas>
<div class="weather"></div>
<div class="hotbar">
    <button data-title="Postać" id="championDataButton"><img src="img/icons/player-icon.png"/></button>
    <button data-title="Chat" id="chatButton"><img src="img/icons/chat-icon.png"/></button>
    <button data-title="Torba" id="eqButton"><img src="img/items/032-Item01.png"/></button>
    <button data-title="Zadania" id="questsButton"><img src="img/icons/quests-icon.png"/></button>
    <button data-title="Umiejętności" id="skilsButton"><img src="img/items/050-Skill07.png"/></button>
    <button data-title="Znajomi" id="friendsButton"><img src="img/items/033-Item02.png"/></button>
    <button data-title="Wyjdź z gry" id="exitGameButton"><img src="img/icons/exit-icon.png"/></button>
</div>
<div class="shadowBox"></div>
<div class="fpsCounter"></div>
<div class="version">v0.0.1</div>