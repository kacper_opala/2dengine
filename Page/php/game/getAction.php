<?php
	session_start();
    require("../mysql.php");
    require("requestParser.php");

	$rezult = new stdClass();
	$rezult->errorCode = 0;
	if(!isset($_SESSION['login'])){
		$rezult->error = "Sesja wygasła";
		$rezult->errorCode = 1;
		die(json_encode($rezult));
	}
	if(!isset($_POST['champion'])){
		$rezult->error = "Brak id postaci";
		$rezult->errorCode = 2;
		die(json_encode($rezult));
	}
	$user = $_SESSION['login']['id'];
    $champion = $_POST['champion'];
    $action = $_POST['action'];

    $sql = "SELECT * FROM action as a WHERE a.id=$action";
	if($rezultat=$sql_conn->query($sql)){
        $actionRow = $rezultat->fetch_assoc();
        $rezult->action = $actionRow;
        if(isset($actionRow['openDialog'])){
            $dialogID = $actionRow['openDialog'];
            $sql = "SELECT text FROM dialogs WHERE id=$dialogID";
            $rezultat=$sql_conn->query($sql);
            $row = $rezultat->fetch_assoc();
            $rezult->dialogText = requestParser::parse($row['text'], $champion);

            $sql = "SELECT n.text, n.action, n.switch
            FROM dialogoptions as o 
            LEFT JOIN dialogoptionsname as n ON n.id = o.optionId 
            WHERE o.dialogId =$dialogID";
            $rezultat=$sql_conn->query($sql);
            $rezult->dialogOptions = [];
            while($row = $rezultat->fetch_assoc()){
                $row['action'] = intval($row['action']);
                //check switch;
                $rezult->dialogOptions[] = $row;
            }
        }if(isset($actionRow['teleport'])){
            $rezult->teleport = json_decode($actionRow['teleport']);
        }if(isset($actionRow['startBattle'])){

        }if(isset($actionRow['changeSwitch'])){

        }if(isset($actionRow['giveItem'])){

        }if(isset($actionRow['takeItem'])){

        }if(isset($actionRow['changeQuest'])){

        }if(isset($actionRow['changeSkin'])){
            $skin = $actionRow['changeSkin'];
            $sql = "UPDATE champions SET skin=$skin WHERE id=$champion";
            //echo $sql;
            if($rezultat=$sql_conn->query($sql)){
                $sql = "SELECT image, ST_X(size) as sizeX, ST_Y(size) as sizeY FROM skins WHERE id=$skin";
                if($rezultat=$sql_conn->query($sql)){
                    $row = $rezultat->fetch_assoc();
                    $rezult->skin = array(
                        "image" => $row['image'],
                        "size" => array( "width"=>intval($row['sizeX']), "height"=>intval($row['sizeY'])),
                    );
                }else{
                    $rezult->error = $sql_conn->error;
                    $rezult->errorCode = 3;
                    die(json_encode($rezult));
                }
            }else{
                $rezult->error = $sql_conn->error;
                $rezult->errorCode = 3;
                die(json_encode($rezult));
            }
        }
    }else{
		$rezult->error = $sql_conn->error;
		$rezult->errorCode = 3;
		die(json_encode($rezult));
	}
	echo json_encode($rezult);
?>