<?php
	session_start();
	require("../mysql.php");
	require_once("../getDataFromUrl.php");

	$rezult = new stdClass();
	$rezult->errorCode = 0;
	if(!isset($_SESSION['login'])){
		$rezult->error = "Sesja wygasła";
		$rezult->errorCode = 1;
		die(json_encode($rezult));
	}
	if(!isset($_POST['champion'])){
		$rezult->error = "Brak id postaci";
		$rezult->errorCode = 2;
		die(json_encode($rezult));
	}
	$user = $_SESSION['login']['id'];
	$champion = $_POST['champion'];
	
	$sql = "SELECT 
		c.name, 
		c.level, 
		c.health, 
		c.mana, 
		c.experience, 
		ST_X(c.position) as positionX, 
		ST_Y(c.position) as positionY, 
		c.strength, 
		c.dexterity, 
		c.intelligence, 
		c.gold,
		c.session,
		
		s.image as skin, 
		ST_X(s.size) as skinWidth,
		ST_Y(s.size) as skinHeight,
		
		m.id as mapId, 
		m.name as mapName, 
		m.background as mapBackground, 
		m.battleBackground as mapBattleBackground, 
		m.layer1 as mapLayer1, 
		m.layer2 as mapLayer2, 
		m.weather as mapWeather, 
		m.collisions as mapCollisions, 
		ST_X(m.size) as mapWidth,
		ST_Y(m.size) as mapHeight,
		st.maxHp as maxHp, st.maxMp as maxMp, st.maxXp as maxXp
	FROM champions as c
	INNER JOIN skins as s ON c.skin = s.id
	INNER JOIN maps as m ON c.map = m.id
	LEFT JOIN statsperlevel as st ON c.level = st.id
	WHERE c.id=$champion and c.user=$user";
	if($rezultat=$sql_conn->query($sql)){
		$count = $rezultat->num_rows;
		if($count == 1){
			$row = $rezultat->fetch_assoc();
			$mapId = $row['mapId'];
			//Load champion
			$rezult->champion = array(
				"name" => $row['name'],
				"level" => intval($row['level']),
				"health" => floatval($row['health']),
				"mana" => floatval($row['mana']),
				"experience" => intval($row['experience']),
				//"position" => array( "x"=>intval($row['positionX']), "y"=>intval($row['positionY'])),
				"strength" => intval($row['strength']),
				"dexterity" => intval($row['dexterity']),
				"intelligence" => intval($row['intelligence']),
				"maxHp" => intval($row['maxHp']),
				"maxMp" => intval($row['maxMp']),
				"maxXp" => intval($row['maxXp']),
				"gold" => intval($row['gold']),
				"skin" => array(
					"image" => $row['skin'],
					"size" => array( "width"=>intval($row['skinWidth']), "height"=>intval($row['skinHeight']))
				)
			);
			if($row['session'] == null){
				$rezult->champion["position"] = array( "x"=>intval($row['positionX']), "y"=>intval($row['positionY']));
			}else{
				$onlinePlayersPos = json_decode(getDataFromUrl('http://localhost:3001/playerPos?id='.$champion));
				if($onlinePlayersPos->errorCode == 0){
					$rezult->champion["position"] = array( "x"=>ceil($onlinePlayersPos->position->x/32), "y"=>ceil($onlinePlayersPos->position->y/32));
				}else{
					$rezult->champion["position"] = array( "x"=>intval($row['positionX']), "y"=>intval($row['positionY']));
				}
			}
			//Load map
			$rezult->map = array(
				"name" => $row['mapName'],
				"background" => $row['mapBackground'],
				"battleBackground" => $row['mapBattleBackground'],
				"layer1" => $row['mapLayer1'],
				"layer2" => $row['mapLayer2'],
				"weather" => $row['mapWeather'],
				"collisions" => json_decode($row['mapCollisions']),
				"size" => array( "width"=>intval($row['mapWidth']), "height"=>intval($row['mapHeight'])),
			);
			//Load friends
			$sql = "SELECT c.name, c.level, m.name as map, f.status, f.CreationTime
			FROM friends as f
			JOIN champions as c ON c.id = f.AddresseeId
			JOIN maps as m ON m.id = c.map
			WHERE f.RequesterId = $champion
			UNION
			SELECT c.name, c.level, m.name, f.status, f.CreationTime
			FROM friends as f
			JOIN champions as c ON c.id = f.RequesterId
			JOIN maps as m ON m.id = c.map
			WHERE f.AddresseeId = $champion";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->friends = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->friends[] = $row;
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Load quests
			$sql = "SELECT q.name, q.value, q.status from quests as q WHERE champion=$champion";
			if($rezultat=$sql_conn->query($sql)){
				include('requestParser.php');
				$rezult->quests = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->quests[] = array(
						"name"=> $row["name"],
						"value"=> requestParser::parse($row['value'], $champion),
						"status"=> $row["status"],
					);
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Load equipment
			$sql = "SELECT i.name, i.icon, i.type, i.stack, i.attributes, i.value, e.slot, e.count, e.active 
			FROM equipment as e
			JOIN items as i ON i.id = e.itemId
			WHERE champion=$champion";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->equipment = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->equipment[] = array(
						"name"=> $row['name'],
						"slot"=> intval($row['slot']),
						"icon"=> $row['icon'],
						"type"=> $row['type'],
						"stack"=> intval($row['stack']),
						"count"=> intval($row['count']),
						"attributes"=> json_decode($row['attributes']),
						"value"=> intval($row['value']),
						"active"=> boolval($row['active']),
					);
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Load switches
			$sql = "SELECT s.name, s.value FROM switches as s WHERE champion=$champion";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->switches = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->switches[] = array(
						"name"=> $row['name'],
						"value"=> json_decode($row['value']),
					);
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Load skils
			$sql = "SELECT s.name, s.icon, s.action, s.cost
			FROM championskils as cs
			JOIN skils as s ON s.id = cs.skill
			WHERE cs.champion=$champion";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->skils = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->skils[] = array(
						"name"=> $row['name'],
						"icon"=> $row['icon'],
						"action"=> json_decode($row['action']),
						"cost"=> intval($row['cost']),
					);
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Load npc
			$sql ="SELECT 
			mn.id, n.name, n.level, n.health, n.attackPower, n.type, n.respawnTime, n.action,
			ST_X(mn.position) as xPos,
			ST_Y(mn.position) as yPos,
			mn.direction,
			s.image, 
			ST_X(s.size) as skinWidth,
			ST_Y(s.size) as skinHeight
			FROM mapnpc as mn
			JOIN npcs as n ON n.id = mn.npcId
			JOIN skins as s on n.skin = s.id
			WHERE mn.mapId=$mapId";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->npcs = [];
				while($row = $rezultat->fetch_assoc()){
					$rezult->npcs[] = array(
						"id"=> intval($row['id']),
						"name"=> $row['name'],
						"level"=> intval($row['level']),
						"health"=> intval($row['health']),
						"attackPower"=> intval($row['attackPower']),
						"type"=> $row['type'],
						"respawnTime"=> intval($row['respawnTime']),
						"action"=> intval($row['action']),
						"position" => array( "x"=>intval($row['xPos']), "y"=>intval($row['yPos'])),
						"direction"=> $row['direction'],
						"skin" => array(
							"image" => $row['image'],
							"size" => array( "width"=>intval($row['skinWidth']), "height"=>intval($row['skinHeight']))
						)
					);
				}
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
			//Update session
			$session = md5(time().$user.$champion);
			$sql = "UPDATE champions SET session='$session' WHERE id=$champion";
			if($rezultat=$sql_conn->query($sql)){
				$rezult->session = $session;				
			}else{
				$rezult->error = $sql_conn->error;
				$rezult->errorCode = 3;
				die(json_encode($rezult));
			}
		}
	}else{
		$rezult->error = $sql_conn->error;
		$rezult->errorCode = 3;
		die(json_encode($rezult));
	}
	echo json_encode($rezult);
?>