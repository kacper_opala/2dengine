<?php
    abstract class requestParser{
        static function standalone(){
            session_start();
            require_once("../mysql.php");
        }
        static function getStringFromDb($sql){
            global $sql_conn;
            if($rezultat=$sql_conn->query($sql)){
                $row = $rezultat->fetch_assoc();
                if($rezultat->num_rows>0)
                    return $row['value'];
                else
                    throw new Exception('Result empty');
            }else{
                throw new Exception('SQL Error');
            }
        }
        static function parseMatch($match, $id){
           try {
                switch($match[0]){
                    case 'champion':
                        return self::getStringFromDb("SELECT ".$match[1]." as value FROM champions WHERE id=$id");
                    break;
                    case 'switch':
                        $jsonPath = explode('.',$match[1]);
                        $json = json_decode(self::getStringFromDb("SELECT value FROM switches WHERE champion=$id and name='".$jsonPath[0]."'"), true);
                        $output = $json[$jsonPath[1]];
                        for($i=2; $i < count($jsonPath); $i++){
                            $output = $output[$jsonPath[$i]];
                        }
                        return $output;
                    break;
                    case 'itemCount':
                        return self::getStringFromDb("SELECT count as value FROM equipment WHERE champion=$id and itemId=".$match[1]);
                    break;
                    case 'requireItem':
                        return $match[2]-self::getStringFromDb("SELECT count as value FROM equipment WHERE champion=$id and itemId=".$match[1]);
                    break;
                    default:
                        return '#undefined';
                    break;
                }
            } catch (Exception $th) {
                return '#undefined';
            }
        }
        public static function parse($text, $id){
            preg_match_all("/(?<=<#)(.*?)(?=\/>)/", $text, $matches);
            $matches = str_replace(' ', '', $matches[0]);
            $results = [];
            foreach($matches as &$match){
                $match = explode(':', $match);
                $results[] = self::parseMatch($match, $id);
            }
            //echo '<pre>';print_r($matches);echo '</pre>';
            $splitedText = preg_split("/(<#)(.*?)(\/>)/",$text);
            $output = "";
            for($i=0; $i < count($splitedText); $i++){
                $output .= $splitedText[$i] .= isset($results[$i])? $results[$i] : "";
            }
            return $output;
        }
    }
    //echo requestParser::parse("Zebrano <# itemCount:1 />/4 Mieczy ",6);
    //echo requestParser::parse("Zbierz jeszcze <# requireItem:1:5 /> warzywa ",6);
    //echo requestParser::parse("Cześć <# champion:name />! Zbierz jeszcze <# switch:quest_rzepa.require /> warzywa ",6);
    //echo requestParser::parse("Cześć Andrzej! Zbierz jeszcze 2 warzywa",6);
?>