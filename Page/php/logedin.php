<script src="js/namegen.js"></script>
<script>
$(function() {
    $("#logoutBtn").click(()=>{
		window.location.href='php/logout.php';
	});
	$("#adminBtn").click(()=>{
		const win = window.open("?admin", "_blank");
		win.focus();
	});
	$("#championsBtn").click(()=>{
		$(".champions").show();
		$(".settings").hide();
		$(".addChampion").hide();
	});
	$("#addChampion").click(()=>{
		$(".addChampion").show();
		$(".settings").hide();
		$(".champions").hide();
	});
	$("#settingsBtn").click(()=>{
		$(".settings").show();
		$(".champions").hide();
		$(".addChampion").hide();
	});
	const generator = NameGen.compile("ss");
	$("#generateNick").click(()=>{
		let nick = generator.toString();
		nick = nick.charAt(0).toUpperCase() + nick.slice(1);
		$("#nickText").val(nick);
	});
});
</script>
<?php 
	if(isset($_POST["addChampion"]))
	{
		$id = $_SESSION['login']['id'];
		$nick = $_POST["nick"];
		$class = $_POST["class"];
		$string = file_get_contents("configs/defaultChampionStats.json");
		$json = json_decode($string, true);
		
		$strength = $json[$class]['strength'];
		$dexterity = $json[$class]['dexterity'];
		$intelligence = $json[$class]['intelligence'];
		

		$sql = "SELECT COUNT(*) as count from champions WHERE user=$id";
		if($rezultat=$sql_conn->query($sql)){
			$count = $rezultat->fetch_assoc()["count"];
			if(strlen($nick) > 3){
				if($count < 5){
					$sql = "SELECT COUNT(*) as count from champions WHERE name='$nick'";
					if($rezultat=$sql_conn->query($sql)){
						$count = $rezultat->fetch_assoc()["count"];
						if($count == 0){
							$sql = "INSERT INTO champions (user, name, strength, dexterity, intelligence) VALUES ($id, '$nick', $strength, $dexterity, $intelligence)";
							if(!$rezultat=$sql_conn->query($sql)){
								echo $sql_conn->error;
							}
						}else{
							echo 'Nick jest zajęty!';
						}
					}else{
						echo $sql_conn->error;
					}
				}else{
					echo "Max champions is 5!";
				}
			}else{
				echo "Nickname must have minimum 3 characters!";
			}
		}else{
			echo $sql_conn->error;
		}
	}
	else if(isset($_POST["changeMail"]))
	{
		$id = $_SESSION['login']['id'];
		$mail = $_POST["mail"];
		$sql = "UPDATE users SET mail='$mail' WHERE id=$id";
		if(!$rezultat=$sql_conn->query($sql))
		{
			echo $sql_conn->error;
		}
	}
	else if(isset($_POST["deleteChampion"]))
	{
		if(!isset($_POST['champion']))
			die("Błąd!");
		$id = $_SESSION['login']['id'];
		$champion = $_POST['champion'];
		$sql = "DELETE FROM champions WHERE user=$id and id=$champion";
		if(!$rezultat=$sql_conn->query($sql)){
			echo $sql_conn->error;
		}
	}
	else if(isset($_POST["changePassword"]))
	{
		$id = $_SESSION['login']['id'];
		$oldPassword = md5($_POST["oldPassword"]);
		$newPassword = $_POST["newPassword"];
		$newPassword2 = $_POST["newPassword2"];
		$sql = "SELECT password from users WHERE id=$id";
		if($rezultat=$sql_conn->query($sql)){
			$row = $rezultat->fetch_assoc();
			if($row['password'] == $oldPassword){
				if(strlen($newPassword)>=3){
					if($newPassword==$newPassword2){
						$newPassword = md5($newPassword);
						$sql = "UPDATE users SET password='$newPassword' WHERE id=$id";
						if(!$rezultat=$sql_conn->query($sql)){
							echo $sql_conn->error;
						}
					}
					else{
						echo "Hasła nie są zgodne!";
					}
				}
				else{
					echo "Hasło jest za krótkie!";
				}
			}
			else{
				echo "Hasło nieprawidłowe!";
			}
		}
		else{
			echo $sql_conn->error;
		}
	}
?>
<div class="menuBar woodenBackground">
	<input type="button" id="championsBtn" value="Postacie" />
	<input type="button" id="settingsBtn" value="Ustawienia" />
	<?php 
		$access=$_SESSION['login']['access']; 
		if($access == 'moderator' || $access == 'admin') echo '<input type="button" id="adminBtn" value="Panel Administracyjny" /> '; ?>
	<input type="button" id="logoutBtn" value="Wyloguj" />
</div>
<div class="champions">
	<table class="championsList">
		<thead>
			<tr>
				<th width="32">L.p</th>
				<th>Nick</th>
				<th>Skin</th>
				<th>Poziom</th>
				<th>Mapa</th>
				<th>Ostatnio w grze</th>
				<th width="128"></th>
			</tr>
		</thead>
		<tbody>
		<?php 
			$sql="SELECT c.id, c.name, s.image, c.experience, st.maxXp, c.level, ST_X(c.position) as positionX, ST_Y(c.position) as positionY, c.lastOnline, m.name as map
				FROM champions AS c 
				INNER JOIN skins AS s ON c.skin = s.id 
				INNER JOIN maps AS m ON c.map = m.id
				LEFT JOIN statsperlevel as st ON c.level = st.id
				WHERE user=".$_SESSION['login']['id']."
				ORDER BY c.id";
			$champions = [];
			if($rezultat=$sql_conn->query($sql))
			{
				if($rezultat->num_rows==0)
				{
					echo "<td colspan='6' style='text-align:center'>Brak postaci</td>";
				}
				else
				{
					$i = 1;
					while($row = $rezultat->fetch_assoc())
					{
						$champions[] = array(
							"name"=>$row['name'],
							"id"=>$row['id']
						);
						echo "<tr>";
						echo "<td>".$i++."</td>";
						echo "<td>".$row['name']."</td>";
						echo "<td>".'<div class="championImage" style="background-image: url('.$row['image'].')"></div>'."</td>";
						echo "<td>".$row['level']."(".($row['experience']/$row['maxXp']*100)."%)"."</td>";
						echo "<td>".$row['map']."(".$row['positionX'].','.$row['positionY'].")"."</td>";
						echo '<td>'.timeDif($row['lastOnline'], $row['id']).'</td>';
						if($access != 'ban') echo "<td>"."<form action='' method='POST'><input type='hidden' name='champion' value='".$row['id']."'/><input type='submit' name='game' value='Graj' /></form>"."</td>";
						echo "</tr>";
					}
				}
				if($rezultat->num_rows<5)
				{
					echo "<tr>";
					echo "<td colspan='7' style='text-align:right;'>"."<input type='button' id='addChampion' value='Dodaj' />"."</td>";
					echo "</tr>";
				}
			}
			else
			{
				echo $sql_conn->error;
			}
		?>
		</tbody>
	</table>
</div>
<div class="addChampion">
	<form action="" method="POST">
		<table class="championsList">
			<tr>
				<td colspan="3">
					<h2 style="text-align:left; display:inline-block">Tworzenie nowej postaci: </h2>
					<input type="text" placeholder="Nick" name="nick" id="nickText" autocomplete="off" />
					<input type="button" id="generateNick" value="Generuj"/><br/>
				</td>
			</tr>
			<tr>
				<td style="width:33.3%">
					<input class="glowingRadio" id="radioWarrior" type="radio" name="class" value="warrior" checked />
					<label for="radioWarrior"><div class="championImage" style="background-image: url(img/skins/014-Warrior02.png)"></div></label>
					<span>Postać bazująca na sile...</span>
				</td>
				<td style="width:33.3%">
					<input class="glowingRadio" id="radioMage" type="radio" name="class" value="mage"/>
					<label for="radioMage"><div class="championImage" style="background-image: url(img/skins/035-Mage03.png)"></div></label>
					<span>Postać bazująca na inteligencji...</span>
				</td>
				<td style="width:33.3%">
					<input class="glowingRadio" id="radioHunter" type="radio" name="class" value="hunter"/>
					<label for="radioHunter"><div class="championImage" style="background-image: url(img/skins/020-Hunter01.png)"></div></label>
					<span>Postać bazująca na zręczności...</span>
				</td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" name="addChampion" value="Utwórz"/></td>
			</tr>
		</table>
	</form>
</div>
<div class="settings">
	<table class="championsList">
		<tr>
			<td>Ustawienia @-mail</td>
			<td>
			<form action="" method="POST">
				<input type="mail" name="mail" placeholder="@-mail" value="<?= $_SESSION['login']['mail'] ?>" />
				<input type="submit" name="changeMail" value="Zapisz" />
			</form>
			</td>
		</tr>
		<tr>
			<td>Usuń postać</td>
			<td>
				<form action="" method="POST">
					<select name="champion" <?= count($champions)==0 ? 'disabled' : '' ?>>
						<?php 
						foreach($champions as &$champion)
						{
							echo "<option value='".$champion['id']."'>".$champion['name']."</option>";
						}
						?>
					</select>
					<input type="submit" name="deleteChampion" value="Usuń" <?= count($champions)==0 ? 'disabled' : '' ?>/>
				</form>
			</td>
		</tr>
		<tr>
			<td>Zmień hasło</td>
			<td>
				<form action="" method="POST">
					<input type="password" name="oldPassword" placeholder="Stare hasło" /><br/>
					<input type="password" name="newPassword" placeholder="Nowe hasło" /><br/>
					<input type="password" name="newPassword2" placeholder="Powtórz hasło" /><br/>
					<input type="submit" name="changePassword" value="Zapisz" />
				</form>
			</td>
		</tr>
	</table>
</div>