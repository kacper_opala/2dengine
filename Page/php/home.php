<?php
	require_once('php/getDataFromUrl.php');
    $onlinePlayersId = json_decode(getDataFromUrl('http://localhost:3001/playerList'));
    function timeDif($timestamp, $playerId){
		GLOBAL $onlinePlayersId;
		try{
			if(in_array($playerId, $onlinePlayersId))
				return 'Teraz gra';
		}catch(Throwable $e){
		
		}
		if($timestamp == null)
			return 'nigdy';
        $now = new DateTime('NOW');
        $time = new DateTime($timestamp);
        $dif = $now->diff($time);

        $total = $dif->y*31556926 + $dif->m*2629743 + $dif->d*86400 + $dif->h*3600 + $dif->i*60 + $dif->s;
        
        if($total < 3600){
			$m = substr($dif->i, -1);
			if($dif->i == 1)
				return $dif->i.' minute temu';
			else if($m > 1 && $m < 5)
				return $dif->i.' minuty temu';
			else
				return $dif->i.' minut temu';
		}else if($total < 86400){
			$h = substr($dif->i, -1);
			if($dif->h == 1)
				$str = $dif->h.' godzine ';
			else if($h > 1 && $h < 5)
				$str = $dif->h.' godziny ';
			else
				$str = $dif->h.' godzin ';
			$m = substr($dif->i, -1);
			if($dif->i == 1)
				$str .= $dif->i.' minute temu';
			else if($m > 1 && $m < 5)
				$str .= $dif->i.' minuty temu';
			else
				$str .= $dif->i.' minut temu';
            return $str;
        }else{
			$t = $dif->y*365 + $dif->m*30 + $dif->d;
			if($t==1)
				return $t.' dzień temu';
			else
				return $t.' dni temu';
		}
    }
?>
<link rel="stylesheet" href="css/home.css" />
<script>
$(function() {
});
</script>
<a href="?" ><img src="img/logo.png" class="logo" /></a>
<div class="container">
	<div class="containerInner">
		<div>
			<p><a href="?news" class="button">Newsy</a>
			<a href="?ranking" class="button">Ranking</a></p>
		</div>
		<hr/>
		<div>
			<?php
				if(isset($_GET['news'])){
					include("php/news.php");
				}
				else if(isset($_GET['ranking'])){
					include("php/rank.php");
				}
				else {
					include("php/news.php");
				}
			?>
		</div>
		<?php
			if(isset($_SESSION["login"]))
				include("php/logedin.php");
			else
				include("php/login.php");
		?>
		<hr/>
		<div class="footer">© 2021 by Grzegorz Furga, Marcin Ledwolorz, Arkadiusz Majak, Kacper Opala</div>
	</div>
</div>
<div style="height: 1px"></div>