<?php
	// Start the session
	session_start();
	require("php/mysql.php");
	function get_enum_values( $table, $field )
	{
		global $sql_conn;
		$type = $sql_conn->query( "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'" )->fetch_assoc()['Type'];;
		preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
		return explode("','", $matches[1]);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>RawEng</title>
		<meta charset="UTF-8" />

		<link rel="icon" type="image/png" href="img/favicon.png"/>
		<link rel="stylesheet" href="css/index.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="js/index.js"></script>
	</head>
	<body>
		<?php
		if(isset($_SESSION["login"]))
		{
			$sql="SELECT u.id, u.login, u.mail, u.access FROM users AS u WHERE id=".$_SESSION["login"]['id'];
			if($rezultat=$sql_conn->query($sql))
			{
				$row = $rezultat->fetch_assoc();
				$_SESSION['login'] = $row;
			}
			else
			{
				echo "<script>alert('$sql_conn->error;')</script>";
			}
		}
		if(isset($_POST["game"]))
		{
			if(isset($_SESSION["login"]))
				include("php/game.php");
			else
				echo "Użytkownik nie zalogowany";
		}
		else if(isset($_GET['admin']))
		{
			include("php/admin.php");
		}
		else
		{
			include("php/home.php");
		}
		?>
	</body>
</html>
