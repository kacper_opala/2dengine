function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie() {
	var user = getCookie("username");
	if (user != "") {
		alert("Welcome again " + user);
	} else {
		user = prompt("Please enter your name:", "");
		if (user != "" && user != null) {
			setCookie("username", user, 365);
		}
	}
}

function MessageBox(text, title = "MessageBox", buttonClass="button")
{
	this.center = function () {
		$(this.window).css({
			top: (window.innerHeight / 2 - $(this.window).height() / 2),
			left: (window.innerWidth / 2 - $(this.window).width() / 2)
		});
	}
	this.title = title;
	this.text = text;
	
	this.window = document.createElement('div');
	$(this.window).css({
		position:"absolute",
		//transform: "translate(-50%, -50%)",
		background: "rgba(0,0,0,0.7)",
		"border-radius": "5px",
		"min-width": "200px",
		"min-height": "100px",
		overflow: "hidden"
	});
	
	this.caption = document.createElement('div');
	$(this.caption).css({
		width: "100%",
		cursor: "pointer",
		color: "white",
		padding: "5px", 
		background: "rgb(0,0,0)"
	});
	$(this.caption).text(this.title);
	$(this.window).append(this.caption);
	
	this.main = document.createElement('div');
	$(this.main).css({
		color: "white",
		padding: "10px", 
	});
	$(this.main).append(this.text);
	$(this.window).append(this.main);
	
	this.button = document.createElement('button');
	$(this.button).addClass(buttonClass);
	$(this.button).css({
		display: "block",
		margin: "5px auto"
	});
	$(this.button).text("OK");
	/*
	$(this.button).attr({
		type: "button",
		value: "OK",
	});*/
	$(this.button).click(()=>{
		//console.log(this);
		this.window.remove();
	});
	$(this.window).append(this.button);
	
	$(this.window).draggable({
	  containment: "body",
	  handle: this.caption
	});
	$("body").append(this.window);
	$(this.window)>$('img').on('load', (event)=> {
		this.center();
	});
	this.center();
}
function championImage(){
	let x=0, y=0, step=0;
	let championAnimationInterval;

	$('.championImage').each(function(){
		const url = $(this).css('background-image').substring(5,$(this).css('background-image').length-2);
		const img = new Image();
		img.onload = ()=>{
			$(this).css({
				'width': img.width/2,
				'height': img.height/2
			});
			$(this).hover(function(){
				championAnimationInterval = setInterval(()=>{
					if(step > 6){
						y+=img.height/2;
						step=0;
					}else{
						step++;
					}
					if(x>=img.width/2*3){
						x=0;
					}else
						x+=img.width/2;
					if(y>=img.height*2)
						y=0;
					$(this).css({'background-position': (-x)+"px "+(-y)+"px"});
				},300);
			},function(){
				clearInterval(championAnimationInterval);
				$(this).css({'background-position': "0px "+"0px"});
				x=0;
				y=0;
			});
		}
		img.src = url;
	});
}
$(function () {
	championImage();
});