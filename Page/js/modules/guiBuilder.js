import { tooltip, progressBar, Form } from './gui.js';
import { player, map } from './dataLoader.js';
import { ping } from './networking.js';
export const gui = {};
export function buildGui(renderer, socket){
	
	//HP bar
    gui.hpBar = new progressBar();
	gui.hpBar.style.boxCss = { left: 32, bottom: 32, width: '25%' };
	$(gui.hpBar.dom).mousemove(function () {
		$(this).css({ opacity: 0.15 });
		tooltip(`${player.attributes.health / 100 * player.attributes.maxHp}/${player.attributes.maxHp}`);
	});
	$(gui.hpBar.dom).mouseleave(function(){
		$(this).css({ opacity: 1 });
		tooltip(null);
	});
	//MP bar
	gui.mpBar = new progressBar();
	gui.mpBar.style.boxCss = { right: 32, bottom: 32, width: '25%' };
	gui.mpBar.style.appearance = "linear-gradient(0deg, rgba(0,0,82,1) 0%, rgba(0,0,255,1) 50%, rgba(0,0,82,1) 100%)";
	$(gui.mpBar.dom).mousemove(function () {
		$(this).css({ opacity: 0.15 });
		tooltip(`${player.attributes.mana / 100 * player.attributes.maxMp}/${player.attributes.maxMp}`);
	});
	$(gui.mpBar.dom).mouseleave(function(){
		tooltip(null);
		$(this).css({ opacity: 1 });
	});
	//player data
	const championDataWindow = new Form(`<table id="championStats" style="width:300px"></table>`,{
		title: 'Postać',
		position: true,
		oncreate:(f)=>{
			$(f.window).hide();
			$(f.window).css({top: 32, left: 32});
		}
	});
	championDataWindow.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	//CHAT
	const chat = new Form(`<div id="chatMessagess" style="width: 25vw; height:200px; overflow-y:scroll; word-wrap: break-word;"></div><input style="width:100%" id="chatMessageText" type="text" placeholder="Wiadomość" />`,{
		title: 'Chat',
		position: true,
		oncreate:(f)=>{
			$(f.window).hide();
			$(f.window).css({
				left: 32, top: window.innerHeight-$(f.window).outerHeight()-32-23-16
			});
		}
	});
	chat.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	$('#chatMessageText').keydown(function(event){
		console.log(event.key);
		switch(event.key){
			case 'Enter':
				if($('#chatMessageText').val().length > 0){
					if($('#chatMessageText').val().substring(0,1) == '/'){
						socket.emit("admin", $('#chatMessageText').val().substring(1));
					}else{
						socket.emit("chat", $('#chatMessageText').val());
					}
					$('#chatMessageText').val("");
				}
				break;
			case 'Escape':
				$('#chatMessageText').blur();
				$(renderer.canvas).click();
				break;
		}
	});
	//player eq
	const eqTable = document.createElement('table');
	eqTable.classList.add("eqTable");
	const rowCount = 10;
	const colCount = 9;
	for(let i=0; i < rowCount; i++){
		const row = document.createElement('tr');
		for(let j=0; j < colCount; j++){
			const col = document.createElement('td');
			row.append(col);
		}
		eqTable.append(row);
	}
	const equipmentWindow = new Form(``,{
		title: 'Torba',
		position: true,
		oncreate:(f)=>{
			$(f.window).hide();
			$(f.main).append(eqTable);
			$(f.window).css({
				left: window.innerWidth-$(f.window).outerWidth()-32, top: window.innerHeight-$(f.window).outerHeight()-32-23-16
			});
			$('.eqTable td').draggable({
				revert: true,
				revertDuration: 0,
				opacity: 0.5,
				containment: eqTable,
				grid: [ 36, 36 ],
				zIndex: 999
			});
			$('.eqTable td').droppable({
				accept: '.eqTable td',
				tolerance: "pointer",
				drop: function(event, ui) {
					const swap = {element: this};
					$('.eqTable td').each(index=>{
						if(this == $('.eqTable td')[index])
							swap.from = index;
						if(ui.draggable[0] == $('.eqTable td')[index])
							swap.to = index;
					})
					const tmp = $('<td>').insertBefore(swap.element);
							$(swap.element).insertAfter($(ui.draggable));
							$(ui.draggable).insertAfter(tmp);
							tmp.remove();
					$.post( "php/game/swapItems.php", {champion: championID, from: swap.from, to: swap.to}, function(data) {}, 'json').done(function(data) {
						if (data.errorCode != 0) {
							const tmp = $('<td>').insertBefore(swap.element);
							$(swap.element).insertAfter($(ui.draggable));
							$(ui.draggable).insertAfter(tmp);
							tmp.remove();
						}
					}).fail(function() {
						const tmp = $('<td>').insertBefore(swap.element);
						$(swap.element).insertAfter($(ui.draggable));
						$(ui.draggable).insertAfter(tmp);
						tmp.remove();
					});
				}
			});
		}
	});
	equipmentWindow.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	//quests
	const questsWindow = new Form(`#Zadania`,{
		title: 'Zadania',
		position: true,
		oncreate:(f)=>{
			$(f.window).hide();
			$(f.window).css({
				left: window.innerWidth-$(f.window).outerWidth()-32, top: 32
			});
		}
	});
	questsWindow.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	//skils
	const sklilsWindow = new Form(`#Skils`,{
		title: 'Umiejętności',
		oncreate:(f)=>{
			$(f.window).hide();
		}
	});
	sklilsWindow.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	//friends
	const friendsWindow = new Form(`#Znajomi`,{
		title: 'Znajomi',
		oncreate:(f)=>{
			$(f.window).hide();
		}
	});
	friendsWindow.onclose(function(a){
		$(a.window).hide();
		return false;
	});
	//Keyboard
	let fpsCounter = false;
	$(window).keydown((event) => {
		switch (event.key) {
			case 'F9':
				if(event.ctrlKey){
					if (fpsCounter) {
						clearInterval(fpsCounter);
						fpsCounter = false;
						$('.fpsCounter').text("");
						$('.fpsCounter').hide();
					} else {
						$('.fpsCounter').show();
						fpsCounter = setInterval(() => {
							$('.fpsCounter').html(`FPS: ${renderer.fps()}<br/>Ping: ${ping}<br/>Position: ${player.position.x},${player.position.y}`);
						}, 300);
					}
				}else{
					$(gui.hpBar.dom).toggle();
					$(gui.mpBar.dom).toggle();
					$('.hotbar').toggle();
				}
				break;
			case 'Enter':
				if(!$("#chatMessageText").is(":focus")){
					$(chat.window).toggle();
					if($(chat.window).is(":visible")){
						$("#chatMessageText").focus();
						$("#chatMessageText").click();
					}
				}
				break;
		}
	});
	//Hotbar title
	$('.hotbar')>$('button').mousemove(function () {
		const title = $(this).data('title');
		tooltip(title);
	});
	$('.hotbar')>$('button').mouseleave(function(){
		tooltip(null);
	});
	//HotBar action
	$('#championDataButton').click(()=>{
		$(championDataWindow.window).toggle();
	});
	$('#chatButton').click(()=>{
		$(chat.window).toggle();
	});
	$('#eqButton').click(()=>{
		$(equipmentWindow.window).toggle();
	});
	$('#questsButton').click(()=>{
		$(questsWindow.window).toggle();
	});
	$('#skilsButton').click(()=>{
		$(sklilsWindow.window).toggle();
	});
	$('#friendsButton').click(()=>{
		$(friendsWindow.window).toggle();
	});
	$('#exitGameButton').click(()=>{
		window.location = location.origin;
	});
}