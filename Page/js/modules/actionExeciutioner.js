import { Form } from './gui.js';
import { lockPlayerControls } from './userControls.js';
import { player } from './dataLoader.js';
import { io } from './networking.js';
export function execAction(target){
    //ajax action
    //console.log(target.name);
    if(target.action == 0)
        return false;
    $.post( "php/game/getAction.php", {champion: championID, action: target.action}, function( data ) {
        console.log(data);
        if(data.action.openDialog && dialogIsOpen==false){
            openDialog(target.name, data.dialogText, data.dialogOptions);
        }if(data.action.teleport){
            io.emit('teleport', data.teleport);
        }if(data.action.changeSkin){
            player.avatar.image.src = data.skin.image;
            player.avatar.width = data.skin.size.width / 4;
            player.avatar.height = data.skin.size.height / 4;
            player.avatar.swidth = data.skin.size.width / 4;
            player.avatar.sheight = data.skin.size.height / 4;
            player.avatar.sx = 0;
            player.avatar.sy = 0;
			player.avatar.orginX = data.skin.size.width / 8 - 16;
            player.avatar.orginY = data.skin.size.height / 4 - 32;
            
            io.emit('changeSkin', data.skin);
        }
    },"json");
}
let dialogIsOpen = false;
function openDialog(name, text, actions){
    dialogIsOpen = true;
    lockPlayerControls(true);
    const options = [];
    actions.forEach(a=>{
        const option = document.createElement('p');
        option.classList.add("dialogOption");
        option.innerHTML = a.text;
        $(option).click(()=>{
            dialogForm.close();
            dialogIsOpen = false;
            lockPlayerControls(false);
            $(renderer.canvas).click();
            execAction({action: a.action, name: name});
        });
        options.push(option);
    });
    const dialogForm = new Form(`${text}<hr/>`,{
        title: name,
        position: true,
        oncreate:(f)=>{
            $(f.button).hide();
            $(f.window).css({
                'width': '60%'
            });
            $(f.main).append(options);
            $(f.window).css({
                'left': (window.innerWidth / 2 - $(f.window).width() / 2),
                'top': window.innerHeight-$(f.window).height()-128
            });
        }
    });
}