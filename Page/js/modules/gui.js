const mousePos = { x: 0, y: 0 };
$(window).mousemove((event) => {
	mousePos.x = event.clientX;
	mousePos.y = event.clientY;
});
export function tooltip(text) {
	const box = $(".tooltip").length ? $(".tooltip") : (()=>{
		$('body').append('<div class="tooltip"></div>');
		$('.tooltip').css({
			"z-index": 999,
			"display": "none",
			"position": "absolute",
			"top": 0,
			"left": 0,
			"background": "rgba(0,0,0,0.7)",
			"border-radius": 5,
			"margin": 0,
			"padding": 5,
			"color": "white"
		});
		return $(".tooltip");
	})();
	box.html(text);
	if (text == null)
		box.hide();
	else {
		box.show();
		box.css({
			top: mousePos.y < window.innerHeight - box.height() - 16 - 32 ? mousePos.y + 16 : mousePos.y - 16 - box.height(),
			left: mousePos.x < window.innerWidth - box.width() - 16 - 32 ? mousePos.x + 16 : mousePos.x - 16 - box.width()
		});
	}
}
export function progressBar(parent="body") {
	this.remove = function () {
		box.remove();
	}
	const box = document.createElement('div');
	const bar = document.createElement('div');
	const text = document.createElement('div');
	$(box).append(bar);
	$(box).append(text);
	this.dom = box;
	this.progress = {
		_value: 0,
		maxValue: 100,
		set value(val) {
			this._value = val;
			if (val >= 0 && val <= this.maxValue) {
				const progress = Math.round(this._value / this.maxValue * 100) + '%';
				$(bar).width(progress);
				$(text).text(progress);
				//console.log(progress);
			}
			else
				throw "Wartość poza zakresem: " + this._value + ' ' + this.maxValue;
			
		},
		get value() {
			return this._value;
        }
	};
	this.progress.value = 100;

	this.style = {
		set boxCss(val) {
			$(box).css(val);
		},
		set barCss(val) {
			$(bar).css(val);
		},
		set textCss(val) {
			$(text).css(val);
		},
		set x(val) {
			$(box).css({left: val});
		},
		get x() {
			return $(box).position().left;
		},
		set y(val) {
			$(box).css({ top: val });
		},
		get y() {
			return $(box).position().top;
		},
		set width(val) {
			$(box).width(val);
		},
		get width() {
			return $(box).width();
		},
		set height(val) {
			$(box).height(val);
		},
		get height() {
			return $(box).height();
		},
		set background(val) {
			$(box).css({ background: val });
		},
		get background() {
			return $(box).css('background');
		},
		set appearance(val) {
			$(bar).css({ background: val });
		},
		get appearance() {
			return $(bar).css('background');
		}
	};
	this.style.width = 200;
	this.style.height = 23;
	this.style.background = "rgba(0,0,0,0.8)";
	this.style.appearance = "linear-gradient(0deg, rgba(82,0,0,1) 0%, rgba(255,0,0,1) 50%, rgba(82,0,0,1) 100%)";

	$(box).css({
		//"pointer-events": "none",
		"position": "absolute",
		"border-radius": "7px",
		"padding": "4px",
		"overflow": "hidden",
		"cursor": "pointer"
	});
	
	$(bar).css({
		"border-radius": "7px",
		"height": "100%",
		"overflow": "hidden"
	});
	$(text).css({
		"position": "absolute",
		"top": "3px",
		"color": "white",
		"overflow": "hidden",
		"text-align": "center",
		"width": "100%",
		"height": "100%",
	});
	$(parent).append(box);
}
//TODO
export function Form(text, opts=null)
{
	this.center = function () {
		$(this.window).css({
			top: (window.innerHeight / 2 - $(this.window).height() / 2),
			left: (window.innerWidth / 2 - $(this.window).width() / 2)
		});
		/*
		else
			$(this.window).css({
				top: opts.position.top,
				left: opts.position.left
				/*bottom: opts.position.bottom,
				right: opts.position.right
			});*/
	}

	let oncloseAction;
	this.onclose = function(action){
		oncloseAction = action;
	};
	this.close = function(){
		if(oncloseAction){
			console.log("closed:", oncloseAction(this));
			if(oncloseAction(this))
				this.window.remove();
		}else{
			this.window.remove();
		}
	}
	
	this.window = document.createElement('div');
	$(this.window).css({
		"position": "absolute",
		"background": "rgba(0,0,0,0.7)",
		"border-radius": "5px",
		"min-width": "200px",
		"min-height": "1em",
		"overflow": "hidden"
	});
	//Uchwyt
	this.caption = document.createElement('div');
	$(this.caption).css({
		width: "100%",
		cursor: "pointer",
		color: "white",
		padding: "1px 1px 1px 5px", 
		background: "rgb(0,0,0)",
		height: 25
	});
	$(this.caption).text(opts?.title);
	$(this.window).append(this.caption);
	$(this.window).draggable({
		containment: "body",
		handle: this.caption
	});
	//Przycisk
	this.button = document.createElement('button');
	$(this.button).addClass('button2');
	$(this.button).css({
		position: "absolute",
		right: 0
	});
	$(this.button).text("X");
	$(this.button).click(()=>{
		this.close();
	});
	$(this.caption).append(this.button);
	//Zawartość
	this.main = document.createElement('div');
	$(this.main).css({
		color: "white",
		padding: "10px", 
	});
	$(this.main).append(text);
	$(this.window).append(this.main);
	
	//Finalizacja
	$("body").append(this.window);
	if(opts?.oncreate)
		opts.oncreate(this);
	if(!opts?.position){
		$(this.window)>$('img').on('load', (event)=> {
			this.center();
		});
		this.center();
	}
}