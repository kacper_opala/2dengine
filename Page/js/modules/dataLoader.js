import { RenderEngine } from './renderer.js'
import { centerMap } from './userControls.js';
import { progressBar, tooltip } from './gui.js';
import { gui, buildGui } from './guiBuilder.js';
export const map = {};
export const player = {};
export const npcs = [];
export function loader(renderer, socket) {
	const loadingBar = new progressBar();
	loadingBar.style.boxCss = { left: '50%', top: '50%', width: '25%', transform: "translate(-50%, -50%)", "z-index": 6 };
	loadingBar.style.appearance = "linear-gradient(0deg, rgba(82,0,66,1) 0%, rgba(162,0,255,1) 50%, rgba(82,0,66,1) 100%)";
	loadingBar.progress.value = 0;

	const loadPrecentage = {
		_value: 0,
		elementCount: Infinity,
		get value(){
			return this._value;
		},
		set value(val){
			this._value = val;
			this.action();
		},
		action(){
			console.log(`Loaded ${this._value} of ${this.elementCount}`);
			loadingBar.progress.value = this._value;
			loadingBar.progress.maxValue = this.elementCount;

			if (this._value >= this.elementCount) {
				$('.shadowBox').animate({ "background-color": "transparent" }, 800);
				$(loadingBar.dom).fadeOut(800, () => { loadingBar.remove(); });
				loadPrecentage.action = ()=>{};
			}
		}
	};

	$.post( "php/game/loadGameData.php", {champion: championID}, function( data ) {
		console.log(data);
		if (data.errorCode == 0) {
			socket.emit("init", data.session);
			socket.on("init", (msg) => {
				loadPrecentage.value++;
			});
			loadPrecentage.elementCount = 4 + data.npcs.length;
			//Mapa
			map.name = data.map.name;
			map.size = data.map.size;
			map.battleBackground = data.map.battleBackground;
			map.collisions = data.map.collisions;
			if (data.map.background != null) {
				map.background = new RenderEngine.Entity(
					renderer,
					0,
					'mapBackground',
					{ x: 0, y: 0 },
					data.map.background,
					{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
					{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
					null,
					() => { loadPrecentage.value++; }
				);
			}
			map.layer1 = new RenderEngine.Entity(
				renderer,
				0,
				'mapLayer1',
				{ x: 0, y: 0 },
				data.map.layer1,
				{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
				{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
				null,
				() => { loadPrecentage.value++; }
			);
			if (data.map.layer2 != null) {
				map.layer2 = new RenderEngine.Entity(
					renderer,
					2,
					'mapLayer2',
					{ x: 0, y: 0 },
					data.map.layer2,
					{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
					{ width: data.map.size.width * 32, height: data.map.size.height * 32 },
					null,
					() => { loadPrecentage.value++; }
				);
			}else{
				loadPrecentage.value++;
			}
			if (data.map.weather != null) {
				map.weather = data.map.weather;
				$('.weather').css({'background-image': `url('${data.map.weather}')`});
			}
			map.pathfinder = {
				grid: map.collisions.length == 0 ? new PF.Grid(map.size.width, map.size.height) : new PF.Grid(map.collisions),
				finder:  new PF.AStarFinder({
					allowDiagonal: false
				})
			}
			//NPC
			data.npcs.forEach((npc) => {
				const tmp = {
					id: npc.id,
					action: npc.action,
					attackPower: npc.attackPower,
					direction: npc.direction,
					health: npc.health,
					level: npc.level,
					name: npc.name,
					level: npc.level,
					position: npc.position,
					respawnTime: npc.respawnTime,
					type: npc.type
				};
				tmp.avatar = new RenderEngine.Entity(
					renderer,
					1,
					'npc',
					{ x: tmp.position.x * 32, y: tmp.position.y * 32 },
					npc.skin.image,
					{ width: npc.skin.size.width / 4, height: npc.skin.size.height / 4 },
					{ width: npc.skin.size.width / 4, height: npc.skin.size.height / 4 },
					{ x: 0, y: 0 },
					() => { loadPrecentage.value++; }
				);
				tmp.avatar.orginX = npc.skin.size.width / 8 - 16;
				tmp.avatar.orginY = npc.skin.size.height / 4 - 32;
				switch (npc.direction) {
					case 'up':
						tmp.avatar.sy = npc.skin.size.height / 4 * 3;
						break;
					case 'down':
						tmp.avatar.sy = 0;
						break;
					case 'left':
						tmp.avatar.sy = npc.skin.size.height / 4 * 1;
						break;
					case 'right':
						tmp.avatar.sy = npc.skin.size.height / 4 * 2;
						break;

				}
				npcs.push(tmp);
				if(tmp.type == 'neutral' || tmp.type == 'aggressive'){
					map.pathfinder.grid.setWalkableAt(tmp.position.x, tmp.position.y, false);
				}
			});
			//Gracz
			player.name = data.champion.name;
			player.session = data.session;
			player.attributes = {
				strength: data.champion.strength,
				dexterity: data.champion.dexterity,
				intelligence: data.champion.intelligence,
				set health(val){
					this._health = val;
					gui.hpBar.progress.value = val;
				}, 
				get health(){
					return this._health;
				},
				set mana(val){
					this._mana = val,
					gui.mpBar.progress.value = val;
				},
				get mana(){
					return this._mana;
				},
				experience: data.champion.experience,
				maxHp: data.champion.maxHp,
				maxMp: data.champion.maxMp,
				maxXp: data.champion.maxXp,
				level: data.champion.level
			};
			player.attributes.health = data.champion.health;
			player.attributes.mana = data.champion.mana;

			player.position = data.champion.position;
			player.direction = "down";
			player.equipment = data.equipment;
			player.skils = data.skils;
			player.switches = data.switches;
			player.avatar = new RenderEngine.Entity(
				renderer,
				1,
				'player',
				{ x: player.position.x * 32, y: player.position.y * 32 },
				data.champion.skin.image,
				{ width: data.champion.skin.size.width / 4, height: data.champion.skin.size.height / 4 },
				{ width: data.champion.skin.size.width / 4, height: data.champion.skin.size.height / 4 },
				{ x: 0, y: 0 },
				() => { loadPrecentage.value++; }
			);
			player.avatar.orginX = data.champion.skin.size.width / 8 - 16;
			player.avatar.orginY = data.champion.skin.size.height / 4 - 32;
			centerMap(player.avatar);
			//Load EQ
			player.equipment.forEach(i=>{
				if(!i.active){
					const slot = $($('.eqTable td')[i.slot]);
					slot.css({'background-image': `url(${i.icon})`});
					$(slot).mousemove(function () {
						tooltip(`${i.name}<br/>Typ: ${i.type}<br/>Ilość: ${i.count}/${i.stack}<br/>Wartość: ${i.value}`);
					});
					$(slot).mouseleave(function(){
						tooltip(null);
					});
				}
			});
			//SetPlayer stas
			$("#championStats").html(`
			<tr>
				<td>Nick</td>
				<td style="text-align:right">${player.name}</td>
			</tr>
			<tr>
				<td>Poziom</td>
				<td style="text-align:right">${player.attributes.level}</td>
			</tr>
			<tr>
				<td>Siła</td>
				<td style="text-align:right">${player.attributes.strength}</td>
			</tr>
			<tr>
				<td>Zręczność</td>
				<td style="text-align:right">${player.attributes.dexterity}</td>
			</tr>
			<tr>
				<td>Inteligencja</td>
				<td style="text-align:right">${player.attributes.intelligence}</td>
			</tr>
			<tr>
				<td colspan="2" id="xpBarHolder" style="height:32px; position:relative">
				</td>
			</tr>
			<tr>
				<td colspan="2" >
				<table class="eqTable" style="margin: 0 auto;">
					<tr>
						<td style="visibility: hidden;"></td>
						<td></td>
						<td style="visibility: hidden;"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td style="visibility: hidden;"></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td style="visibility: hidden;"></td>
						<td></td>
						<td style="visibility: hidden;"></td>
					</tr>
				</table>
				</td>
			</tr>`);
			gui.xpBar = new progressBar("#xpBarHolder");
			gui.xpBar.progress.maxValue = player.attributes.maxXp;
			gui.xpBar.progress.maxValue = player.attributes.experience;
			gui.xpBar.style.boxCss = { width: '250px', left: "50%", top: "0",  transform: "translateX(-50%)"};
			gui.xpBar.style.appearance = "linear-gradient(0deg, rgba(125,125,0,1) 0%, rgba(255,188,0,1) 50%, rgba(125,125,0,1) 100%)";
			$(gui.xpBar.dom).mousemove(function () {
				$(this).css({ opacity: 0.15 });
				tooltip(`${player.attributes.experience}/${player.attributes.maxXp}`);
			});
			$(gui.xpBar.dom).mouseleave(function(){
				$(this).css({ opacity: 1 });
				tooltip(null);
			});
		} else {
			console.error(data);
		}
	}, "json");
}