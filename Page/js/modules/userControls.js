import { map, player, npcs } from './dataLoader.js';
import { tooltip } from './gui.js';
import { players } from './networking.js';
import { execAction } from './actionExeciutioner.js';
import { RenderEngine } from './renderer.js'
export let centerMap;
export let centerMapSlow;
export function lockPlayerControls(bool){
	lockControls = bool;
}
let lockControls = false;
export function userControls(renderer) {
	const moveVector = {};
	let centerOnPlayer = true;
	//lockControls = false;
	const moveTime = 32 * 6;
	const moveTimeDivided = moveTime / 32;
	
	function move(dir) {
		let i = 0;
		let moveInterval = setInterval(() => {
			switch (dir) {
				case 'up':
					--player.avatar.y;
					break;
				case 'down':
					++player.avatar.y;
					break;
				case 'left':
					--player.avatar.x;
					break;
				case 'right':
					++player.avatar.x;
					break;
			}
			if (++i >= 32) {
				clearInterval(moveInterval);
				if (!moveVector.up && !moveVector.down && !moveVector.left && !moveVector.right)
					player.avatar.sx = 0;
			}
		}, moveTimeDivided);
	}
	function animateMove() {
		if (player.avatar.sx < player.avatar.image.width / 4 * 3)
			player.avatar.sx += player.avatar.image.width / 4;
		else {
			player.avatar.sx = 0;
		}
	}
	centerMap = function (entity) {
		const cameraCenterX = renderer.Camera.width / 2;
		const cameraCenterY = renderer.Camera.height / 2;
		const mapW = map.size.width * 32;
		const mapH = map.size.height * 32;
		if (mapW > renderer.Camera.width) {
			if (entity.x > cameraCenterX && entity.x < mapW - cameraCenterX) {
				renderer.Camera.x = entity.x - cameraCenterX;
			} else if (entity.x < cameraCenterX) {
				renderer.Camera.x = 0;
			} else if (entity.x > mapW - cameraCenterX) {
				renderer.Camera.x = mapW - renderer.Camera.width;
			}
		} else {
			renderer.Camera.x = -cameraCenterX + mapW / 2;
        }
		if (mapH > renderer.Camera.height) {
			if (entity.y > cameraCenterY && entity.y < mapH - cameraCenterY) {
				renderer.Camera.y = entity.y - cameraCenterY;
			} else if (entity.y < cameraCenterY) {
				renderer.Camera.y = 0;
			} else if (entity.y > mapH - cameraCenterY) {
				renderer.Camera.y = mapH - renderer.Camera.height;
			}
		} else {
			renderer.Camera.y = -cameraCenterY + mapH / 2;
        }
	}
	centerMapSlow = function(entity, speed=300, backToPlayer = false){
		centerOnPlayer = backToPlayer;
		const cameraCenterX = renderer.Camera.x + renderer.Camera.width / 2;
		const cameraCenterY = renderer.Camera.y + renderer.Camera.height / 2;
		$({x: cameraCenterX, y: cameraCenterY}).animate({x: entity.x, y: entity.y}, {
			duration: speed,
			easing: 'linear',
			step: (now, fx)=>{
				if(fx.prop == 'x')
					centerMap({x:now, y: renderer.Camera.y + renderer.Camera.height / 2})
				else if(fx.prop == 'y')
					centerMap({x:renderer.Camera.x + renderer.Camera.width / 2, y: now})
			}
		});
	}
	function isColide(dir) {
		try{
			switch (dir) {
				case 'up':
					if (player.position.y <= 0) return true;
					if (npcs.findIndex((npc) => { return npc.position.x == player.position.x && npc.position.y == player.position.y - 1 && (npc.type == 'neutral' || npc.type == 'aggressive')}) != -1) return true;
					if (map.collisions[player.position.y - 1][player.position.x]) return true;
					break;
				case 'down':
					if (player.position.y >= map.size.height - 1) return true;
					if (npcs.findIndex((npc) => { return npc.position.x == player.position.x && npc.position.y == player.position.y + 1 && (npc.type == 'neutral' || npc.type == 'aggressive')}) != -1) return true;
					if (map.collisions[player.position.y + 1][player.position.x]) return true;
					break;
				case 'left':
					if (player.position.x <= 0) return true;
					if (npcs.findIndex((npc) => { return npc.position.x == player.position.x - 1 && npc.position.y == player.position.y && (npc.type == 'neutral' || npc.type == 'aggressive')}) != -1) return true;
					if (map.collisions[player.position.y][player.position.x - 1]) return true;
					break;
				case 'right':
					if (player.position.x >= map.size.width - 1) return true;
					if (npcs.findIndex((npc) => { return npc.position.x == player.position.x + 1 && npc.position.y == player.position.y && (npc.type == 'neutral' || npc.type == 'aggressive')}) != -1) return true;
					if (map.collisions[player.position.y][player.position.x + 1]) return true;
					break;
			}
		}catch(e){
			return false;
		}
		return false;
		//OLD collisions system
		//if (map.collisions.findIndex((element) => { return element.x == player.position.x + 1 }) != -1) return true;
	}
	setInterval(() => {
		if(lockControls)
			return;
		if (moveVector.up) {
			centerOnPlayer = true;
			player.avatar.sy = player.avatar.image.height / 4 * 3;
			if (!isColide('up')) {
				--player.position.y;
				player.direction = "up";
				move('up');
				animateMove();
			}else{
				player.avatar.sx = 0;
			}
		}
		if (moveVector.down) {
			centerOnPlayer = true;
			player.avatar.sy = 0;
			if (!isColide('down')) {
				++player.position.y;
				player.direction = "down";
				move('down');
				animateMove();
			}else{
				player.avatar.sx = 0;
			}
		}
		if (moveVector.left) {
			centerOnPlayer = true;
			player.avatar.sy = player.avatar.image.height / 4;
			if (!isColide('left')) {
				--player.position.x;
				player.direction = "left";
				move('left');
				animateMove();
			}else{
				player.avatar.sx = 0;
			}
		}
		if (moveVector.right) {
			centerOnPlayer = true;
			player.avatar.sy = player.avatar.image.height / 4 * 2;
			if (!isColide('right')) {
				++player.position.x;
				player.direction = "right";
				move('right');
				animateMove();
			}else{
				player.avatar.sx = 0;
			}
		}
		if(centerOnPlayer)
			centerMapSlow(player.avatar, moveTime, true);
	}, moveTime);

	$(window).resize(() => {
		const cameraCenterX = renderer.Camera.x + renderer.Camera.width / 2;
		const cameraCenterY = renderer.Camera.y + renderer.Camera.height / 2;
		centerMap({x: cameraCenterX, y: cameraCenterY});
		
	});
	let clickTarget = renderer.canvas;
	$(window).click((event)=>{
		clickTarget = event.target;
	});
	$(window).keydown((event) => {
		if(clickTarget == renderer.canvas){
			stopPathMover();
			RenderEngine.RemoveEntity(renderer, 0, 'pathDot');
			switch (event.key) {
				//Sterowanie postacią
				case 'w':
				case 'W':
					moveVector.up = true;
					moveVector.left = false;
					moveVector.right = false;
					break;
				case 'a':
				case 'A':
					moveVector.left = true;
					moveVector.up = false;
					moveVector.down = false;
					break;
				case 's':
				case 'S':
					moveVector.down = true;
					moveVector.left = false;
					moveVector.right = false;
					break;
				case 'd':
				case 'D':
					moveVector.right = true;
					moveVector.up = false;
					moveVector.down = false;
					break;
				//GUI
				
				//DEBUG
				case 'l':
					lockControls = !lockControls;
					break;
			}
		}
	});
	$(window).keyup((event) => {
		switch (event.key) {
			case 'w':
			case 'W':
				moveVector.up = false;
				break;
			case 'a':
			case 'A':
				moveVector.left = false;
				break;
			case 's':
			case 'S':
				moveVector.down = false;
				break;
			case 'd':
			case 'D':
				moveVector.right = false;
		}
	});
	$(window).blur(() => {
		moveVector.up = false;
		moveVector.left = false;
		moveVector.down = false;
		moveVector.right = false;
	});
	renderer.onHover((entity) => {
		const current = entity.find(e => e.id == 'npc') || entity.find(e => e.id.substring(0,10) == 'netPlayer_') || entity.find(e => e.id == 'player')
		if (current?.id == 'player'){
			tooltip('Ja');
		}else if (current?.id == 'npc') {
			const npc = npcs.find((n) => n.avatar == current)
			tooltip(npc.name);
			if(npc.type == 'door')
				npc.avatar.sy = 48;
		}else if (current?.id.substring(0,10) == 'netPlayer_') {
			const player = players.find((p) => p.avatar == current)
			tooltip(`${player.name} (lvl: ${player.level})`);
		}else{
			tooltip(null);
			npcs.forEach(n=>{if(n.type == 'door')n.avatar.sy=0});
		}
	});
	let pathmover = null;
	renderer.onClick((entity, mouse) => {
		//centerMapSlow(mouse);
		const current = entity.find(e => e.id == 'npc') || entity.find(e => e.id.substring(0,10) == 'netPlayer_') || entity.find(e => e.id == 'player')
		if (current?.id == 'player'){
			console.log(player);
		}else if (current?.id == 'npc') {
			const npc = npcs.find((n) => n.avatar == current)
			console.log(npc);
			const distance = Math.sqrt(Math.pow(npc.position.x - player.position.x, 2)+Math.pow(npc.position.y - player.position.y, 2));//RenderEngine.Distance(renderer,player.avatar, npc.avatar);
			console.log(distance);
			if(distance < 2){
				execAction(npc);
			}else{
				const pos = [
					{x: Math.floor(mouse.x/32),y: Math.floor(mouse.y/32)},
					{x: Math.floor(mouse.x/32),y: Math.floor(mouse.y/32)+1},
					{x: Math.floor(mouse.x/32)-1,y: Math.floor(mouse.y/32)},
					{x: Math.floor(mouse.x/32)+1,y: Math.floor(mouse.y/32)},
					{x: Math.floor(mouse.x/32),y: Math.floor(mouse.y/32)-1}
				]
				let i=0;
				while(!startPathMover(pos[i++]) && i < pos.length);
			}
		}else if (current?.id.substring(0,10) == 'netPlayer_') {
			const player = players.find((p) => p.avatar == current)
			console.log(player);
		}else{
			const pos = {x: Math.floor(mouse.x/32),y: Math.floor(mouse.y/32)}
			startPathMover(pos);
		}
	});
	function startPathMover(pos){
		RenderEngine.RemoveEntity(renderer, 0, 'pathDot');
		const grid = map.pathfinder.grid.clone();
		const path = map.pathfinder.finder.findPath(player.position.x, player.position.y, pos.x, pos.y, grid);
		//console.log(path);
		if(path.length == 0)
			return false;
		path.forEach(p=>{
			new RenderEngine.Entity(
				renderer,
				0,
				'pathDot',
				{ x: p[0] * 32, y: p[1] * 32 },
				'img/pathDot.png',
				{ width: 32, height: 32 },
				{ width: 32, height: 32 },
				{ x: 0, y: 0 },
				null
			);
		});
		let i = 2;
		stopPathMover();
		function moveByPath(i){
			if(path[i-1][0] > path[i][0]){
				moveVector.left = true;
				moveVector.up = false;
				moveVector.right = false;
				moveVector.down = false;
			}else if(path[i-1][0] < path[i][0]){
				moveVector.right = true;
				moveVector.up = false;
				moveVector.down = false;
				moveVector.left = false;
			}else if(path[i-1][1] > path[i][1]){
				moveVector.up = true;
				moveVector.left = false;
				moveVector.down = false;
				moveVector.right = false;
			}else if(path[i-1][1] < path[i][1]){
				moveVector.down = true;
				moveVector.up = false;
				moveVector.left = false;
				moveVector.right = false;
			}
		}
		moveByPath(1);
		pathmover = setInterval(function(){
			if(i>=path.length){
				stopPathMover();
				RenderEngine.RemoveEntity(renderer, 0, 'pathDot');
				return;
			}
			//console.log(path[i]);
			moveByPath(i++);
		},moveTime);
		return true;
	}
	function stopPathMover(){
		clearInterval(pathmover);
		moveVector.up = false;
		moveVector.left = false;
		moveVector.down = false;
		moveVector.right = false;
	}
}
function mobileCheck() {
	let check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};