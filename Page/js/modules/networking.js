import { RenderEngine } from './renderer.js'
import {player} from './dataLoader.js';
export let ping = 0;
export const players = [];
export let io;
export function networking(renderer, socket) {
	io = socket
	socket.on('connect', function () {
		console.log("Socket Connected");
	});
	let pingTime;
	setInterval(function() {
		pingTime = Date.now();
		socket.emit('ping');
	}, 2000);
	socket.on('ping', function() {
		ping = Date.now() - pingTime;
	});
	socket.on('chat', function(msg) {
		console.log(msg);
		const time = new Date(msg.time);
		const timeString = `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
		$("#chatMessagess").append(`<p><b style="color:#808080" title="${timeString}">${msg.player}:</b> ${msg.msg}</p>`);
		$("#chatMessagess").scrollTop($("#chatMessagess")[0].scrollHeight);
	});
	socket.on('changeSkin', function(msg) {
		console.log(msg);
		if(player.session == msg.player){
			return;
		}
		const netPlayerData = players.find((p)=> p.session == msg.player);
		console.log(netPlayerData);
		if(netPlayerData){
			netPlayerData.avatar.image.src = msg.skin.image;
            netPlayerData.avatar.width = msg.skin.size.width / 4;
            netPlayerData.avatar.height = msg.skin.size.height / 4;
            netPlayerData.avatar.swidth = msg.skin.size.width / 4;
            netPlayerData.avatar.sheight = msg.skin.size.height / 4;
            netPlayerData.avatar.sx = 0;
            netPlayerData.avatar.sy = 0;

			netPlayerData.avatar.orginX = msg.skin.size.width / 8 - 16;
			netPlayerData.avatar.orginY = msg.skin.size.height / 4 - 32;
		}
	});
	socket.on('game', function(msg) {
		msg.forEach(netPlayer => {
			if(player.session == netPlayer.session){
				return;
			}
			const netPlayerData = players.find((p)=> p.session == netPlayer.session);
			if(netPlayerData == undefined){
				//console.log(msg);
				netPlayer.avatar = new RenderEngine.Entity(
					renderer,
					1,
					'netPlayer_'+netPlayer.session,
					{ x: netPlayer.position.x * 32, y: netPlayer.position.y * 32 },
					netPlayer.skin,
					{ width: netPlayer.skinSize.x / 4, height: netPlayer.skinSize.y / 4 },
					{ width: netPlayer.skinSize.x / 4, height: netPlayer.skinSize.y / 4 },
					{ x: 0, y: 0 },
					null
				);
				netPlayer.avatar.orginX = netPlayer.skinSize.x / 8 - 16;
				netPlayer.avatar.orginY = netPlayer.skinSize.y / 4 - 32;
				players.push(netPlayer);
			}else{
				//console.log(msg);
				netPlayerData.avatar.x = netPlayer.position.x;
				netPlayerData.avatar.y = netPlayer.position.y;
				netPlayerData.avatar.sx = netPlayer.frame;
				netPlayerData.avatar.sy = netPlayer.direction;
			}
		});
		for(let i = 0; i<players.length; i++){
			if(msg.find(p=>p.session ==players[i].session) == undefined){
				RenderEngine.RemoveEntity(renderer,1,"netPlayer_"+players[i].session);
				players.splice(i, 1);
				break;
			}
		}
		const send = {position: {x: player.avatar.x, y: player.avatar.y}, direction: player.avatar.sy, frame: player.avatar.sx};
		socket.emit("game", send);
	});
	socket.on('teleport', function(msg) {
		console.log('teleport', msg);
		if(msg == true)
			location.reload();
	});
	socket.on('weather', function(msg) {
		console.log(msg);
		if(msg == 'null')
			$('.weather').css({'background-image': ''});
		else
			$('.weather').css({'background-image': `url('${msg}')`});
	});
	socket.on('disconnect', function() {
		renderer.stop = true;
		onDisconnect("Utracono połączenie");
	});
	socket.on('connect_error', onError);
	socket.on('connect_failed', onError);
	socket.on('error', onError);
}
function onError(err){
	console.log(err);
	$('.shadowBox').animate({ "background-color": "black" }, 800);
	$('.shadowBox').text("Błąd: "+err);
}
function onDisconnect(reason){
	$('.shadowBox').animate({ "background-color": "black" }, 800);
	$('.shadowBox').text(reason);
}