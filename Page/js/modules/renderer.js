export const RenderEngine = {
	Init: function(id, fps=60, size=null){
		this.canvas = document.getElementById(id);
		this.ctx = this.canvas.getContext("2d");
		this.size = size;
		if(size){
			this.canvas.width = size.width;
			this.canvas.height = size.height;
		}else{
			this.canvas.width = this.canvas.parentElement.clientWidth;
			this.canvas.height = this.canvas.parentElement.clientHeight;
		}
		this.Camera = null;
		this.entList0 = [];
		this.entList1 = [];
		this.entList2 = [];
		this.stop = false;
		const frameTime = 1000/fps;
		let deltaTime = 0;
		this.fps = function(){
			//return deltaTime;
			const _fps = 1000/deltaTime;
			return Math.round((_fps+Number.EPSILON)*10)/10;
		};
		this.onClick = (callback) => {
			this.canvas.addEventListener('click', (event) => {
				//console.log(event);
				const x = event.offsetX + this.Camera.x;
				const y = event.offsetY + this.Camera.y;
				const elements = [];
				this.entList0.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				this.entList1.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				this.entList2.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				callback(elements, {x:x, y:y});
			}, false);
		};
		this.onHover = (callback) => {
			this.canvas.addEventListener('mousemove', (event) => {
				const x = event.offsetX + this.Camera.x;
				const y = event.offsetY + this.Camera.y;
				const elements = [];
				this.entList0.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				this.entList1.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				this.entList2.forEach(function (element) {
					if (y > element.y - element.orginY &&
						y < element.y - element.orginY + element.height &&
						x > element.x - element.orginX &&
						x < element.x - element.orginX + element.width) {
						elements.push(element);
					}
				});
				callback(elements, {x:x, y:y});
			}, false);
		};
		this.pollEvent = null;
		this.update = () => {
			const start = performance.now();
			if(this.stop) return;
			//this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			this.ctx.fillStyle = "#392553";
			this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
			//this.entList0.sort((a, b) => (a.y > b.y) ? 1 : -1);	
			this.entList0.forEach(function (ent) {
				ent.update();
			});
			this.entList1.sort((a, b) => (a.y - b.y) || (a.id =='player'? 1 : -1) || 1 );
			this.entList1.forEach(function(ent){
				ent.update();
			});
			//this.entList2.sort((a, b) => (a.y > b.y) ? 1 : -1);
			this.entList2.forEach(function(ent){
				ent.update();
			});
			const end = performance.now() - start;
			deltaTime = Math.max(frameTime - end, 0);
			if(this.pollEvent)
				this.pollEvent(deltaTime);
			setTimeout(this.update, deltaTime);
		};
		this.update();
	},
	Camera: function(renderer){
		this.x = 0;
		this.y = 0;
		this.width = renderer.canvas.width;
		this.height = renderer.canvas.height;
		let _zoom = 1;
		this.Zoom = function (scale = null) {
			if (scale == null)
				return _zoom;
			else {
				renderer.ctx.setTransform(1, 0, 0, 1, 0, 0);
				renderer.ctx.scale(scale, scale);
				_zoom = scale;
			}
		}
		this.Resize = (size)=>{
			renderer.ctx.fillStyle = "#392553";
			renderer.size = size;
			renderer.canvas.width = size.width;
			renderer.canvas.height = size.height;
			this.width = renderer.canvas.width;
			this.height = renderer.canvas.height;
		}
		if(!renderer.size){
			window.addEventListener('resize', () => {
				renderer.ctx.fillStyle = "#392553";
				renderer.canvas.width = window.innerWidth;
				renderer.canvas.height = window.innerHeight;
				this.width = renderer.canvas.width;
				this.height = renderer.canvas.height;
				/*this.Zoom(1);
				if (this.width <= 1920)
					this.Zoom(1);
				else
					this.Zoom(1.5);*/
			}, true);
		}
		renderer.Camera = this;
	},
	Entity: function(renderer,
					layer, 
					id, 
					position=null, 
					url=null, 
					size=null, 
					viewport=null, 
					offset=null, 
					onload=null){
		this.id = id;
		this.layer = layer;
		if(size == null){
			this.width = 0;
			this.height = 0;
		}else{
			this.width = size.width;
			this.height = size.height;
		}
		if(viewport == null){
			this.swidth = 0;
			this.sheight = 0;
		}else{
			this.swidth = viewport.width;
			this.sheight = viewport.height;
		}
		if(offset == null){
			this.sx = 0;
			this.sy = 0;
		}else{
			this.sx = offset.x;
			this.sy = offset.y;	
		}
		this.speedX = 0;
		this.speedY = 0; 
		this.orginX = 0;
		this.orginY = 0;
		if(position == null){
			this.x = 0;
			this.y = 0;
		}else{
			this.x = position.x; //- renderer.Camera.x;
			this.y = position.y; //- renderer.Camera.y;
		}
		this.image = null;
		if(url != null){
			this.image = new Image();
			this.image.src = url;
			this.image.onload = function(){
				renderer.ctx.drawImage(this,0,0,this.swidth,this.sheight,this.x+this.orginX,this.y+this.orginY,this.width,this.height);
				if(onload != null)
					onload();
			};
			this.image.onerror = () => {
				console.log(this.image.src+' not found!');
				this.image.src = null;
				this.update = function(){};
			};
		}
		this.update = function() {
			let tmp_x = this.x - renderer.Camera.x - this.orginX;
			let tmp_y = this.y - renderer.Camera.y - this.orginY;
			if(this.image != null)
				renderer.ctx.drawImage(
					this.image,
					this.sx,this.sy,
					this.swidth,this.sheight,
					tmp_x, tmp_y,
					this.width, this.height
				);
		};
		if(this.layer == 0)
			renderer.entList0.push(this);
		else if(this.layer == 1)
			renderer.entList1.push(this);
		else if(this.layer == 2)
			renderer.entList2.push(this);
	},
	RemoveAllEntity: function(renderer){
		renderer.entList0 = [];
		renderer.entList1 = [];
		renderer.entList2 = [];
	},
	RemoveEntity: function(renderer, layer, id){
		if(layer == 0){
			renderer.entList0 = renderer.entList0.filter(e=>e.id != id);
		}
		else if(layer == 1){
			renderer.entList1 = renderer.entList1.filter(e=>e.id != id);
		}
		if(layer == 2){
			renderer.entList2 = renderer.entList2.filter(e=>e.id != id);
		}
		return true;
	},
	RemoveEntityLambda: function(renderer, layer, lambda){
		if(layer == 0){
			renderer.entList0 = renderer.entList0.filter(lambda);
		}
		else if(layer == 1){
			renderer.entList1 = renderer.entList1.filter(lambda);
		}
		if(layer == 2){
			renderer.entList2 = renderer.entList2.filter(lambda);
		}
		return true;
	},
	Collision: function(renderer, ent1, ent2){
		const ent1_left = ent1.x;
        const ent1_right = ent1.x + (ent1.width);
        const ent1_top = ent1.y;
        const ent1_bottom = ent1.y + (ent1.height);
        const ent2_left = ent2.x;
        const ent2_right = ent2.x + (ent2.width);
        const ent2_top = ent2.y;
        const ent2_bottom = ent2.y + (ent2.height);
		let collision = true;
        if (
			(ent1_bottom < ent2_top) ||
            (ent1_top > ent2_bottom) ||
            (ent1_right < ent2_left) ||
            (ent1_left > ent2_right)) {
           collision = false;
        }
        return collision;
	},
	Distance: function(renderer, ent1, ent2){
		const ent1X = (ent1.x + ent1.width)/2;
		const ent1Y = (ent1.y + ent1.height)/2;
		const ent2X = (ent2.x + ent2.width)/2;
		const ent2Y = (ent2.y + ent2.height)/2;
		return Math.sqrt(Math.pow(ent1X-ent2X,2)+Math.pow(ent1Y-ent2Y,2));
	}
};