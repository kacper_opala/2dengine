import {RenderEngine} from './modules/renderer.js'
import {map, player, npcs, loader} from './modules/dataLoader.js';
import {buildGui} from './modules/guiBuilder.js';
import {userControls} from './modules/userControls.js';
import {networking} from './modules/networking.js';

$(function () {
	$(window).contextmenu((event) => {
		//event.preventDefault();
	});
	const socket = io.connect(location.origin+":3000");
	const renderer = new RenderEngine.Init('renderer');
	const camera = new RenderEngine.Camera(renderer);

	///////////////////DEBUG///////////////////
	const d = {};
	d.renderer = renderer;
	d.camera = camera;
	d.socket = socket;
	d.player = player;
	d.map = map;
	d.npcs = npcs;
	///////////////////DEBUG///////////////////

	loader(renderer, socket);
	buildGui(renderer, socket);
	networking(renderer, socket);
	userControls(renderer);
	
	window.d = d;
});
