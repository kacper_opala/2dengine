const http = require("http").createServer();
const io = require("socket.io")(http, {
  cors: {
    origin: "*",
  },
});
const { RSA_X931_PADDING } = require("constants");
const mysql = require("mysql");
const readline = require('readline');
const port = process.env.PORT || 3000;

const express = require('express');
const app = express();

const players = [];
const intervals = [];

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (input) => {
  const command = input.split(' ');
  switch(command[0]){
    case 'stop':
      stopServer();
      break;
    default:
      console.log('command '+command[0]+' not found!');
      break;
  }
});
function stopServer(){
  console.log("Stoping serwer - saveing data!");
  clearInterval(autoSave);
  http.close();
  appServer.close();
  players.forEach(p=>p.socket.disconnect());
  io.close();
  sql.end();
  rl.close();
  rl.removeAllListeners();
}

const autoSave = setInterval(function(){
  console.log(`autosaveing data - ${players.length} is online`);
  players.forEach(player=>{
    sql.query(
      `UPDATE champions SET map=${player.champion.map
      }, position=Point(${Math.ceil(
        player.champion.position.x / 32
      )},${Math.ceil(player.champion.position.y / 32)}) WHERE id='${player.champion.id
      }'`,
      function (err, result, fields) {
        if (err) console.log(err);
      }
    );
  });
},1000*60*15);

io.on("connection", function (socket) {
  //console.log("new connection");
  socket.on("init", function (msg) {
    sql.query(
      `SELECT c.id, u.access, c.name, c.level, c.health, c.mana, c.experience, c.position,	c.strength, c.dexterity, c.intelligence, c.gold, c.map, s.image as skin, s.size as skinSize, st.maxHp as maxHp, st.maxMp as maxMp, st.maxXp as maxXp
              FROM champions as c
              INNER JOIN skins as s ON c.skin = s.id
              LEFT JOIN statsperlevel as st ON c.level = st.id 
              LEFT JOIN users as u ON c.user = u.id
              WHERE session='${msg}'`,
      function (err, result, fields) {
        if (err) throw err;
        console.log(
          `Player ${result[0].name}(${result[0].id}) entered map ${result[0].map} at ${result[0].position.x},${result[0].position.y}`
        );
        //console.log(result);
        if(result[0].access == 'ban'){
          socket.disconnect(true);
        }
        const player = {
          champion: result[0],
          socket: socket,
        };
        player.champion.session = msg;
        player.champion.direction = "down";
        player.champion.frame = 0;
        socket.join(`map_${player.champion.map}`);
        const concurentSession = players.find(
          (p) => p.champion.id == player.champion.id
        );
        if (concurentSession) {
          concurentSession.socket.disconnect(true);
        }
        players.push(player);
        //console.log(players);
        const interval = setInterval(function () {
          const allplayers = players.map((a) => a.champion);
          const send = allplayers.filter(p=>p.map == player.champion.map);
          io.to(`map_${player.champion.map}`).emit("game", send);
        }, 30);
        intervals.push({ player: player, interval: interval });
      }
    );
    socket.emit("init");
  });
  socket.on("ping", function () {
    socket.emit("ping");
  });
  socket.on("chat", function (msg) {
    const player = players.find((p) => p.socket == socket);
    const send = {
      player: player.champion.name,
      msg: msg,
      time: new Date().getTime(),
    };
    console.log(
      `Message on ${Array.from(socket.rooms)[1]}, from ${send.player
      } at ${new Date(send.time).getHours()}:${new Date(
        send.time
      ).getMinutes()}:${new Date(send.time).getSeconds()} - ${send.msg}`
    );
    io.to(`map_${player.champion.map}`).emit("chat", send);
  });
  socket.on("game", function (msg) {
    const player = players.find((p) => p.socket == socket);
    if(!player.teleport){
      player.champion.position = msg.position;
      player.champion.direction = msg.direction;
      player.champion.frame = msg.frame;
    }
  });
  socket.on("changeSkin", function (msg) {
    const player = players.find((p) => p.socket == socket);
    player.champion.skin = msg.image;
    player.champion.skinSize = {x: msg.size.width, y: msg.size.height};
    const send = {
      player: player.champion.session,
      skin: msg
    };
    socket.emit("changeSkin", send);
  });
  socket.on("teleport", function (msg) {
    const player = players.find((p) => p.socket == socket);
    //console.log(msg);
    player.teleport = true;
    player.champion.map = msg.map;
    player.champion.position = {x: msg.x*32, y: msg.y*32};
    socket.emit("teleport", true, function(){
      socket.disconnect();
    });
  });
  socket.on("admin", function (msg) {
    const player = players.find((p) => p.socket == socket);
    const send = {
      player: 'System',
      msg: null,
      time: new Date().getTime(),
    };
    if(player.champion.access == 'moderator' || player.champion.access == 'admin'){
      const command = msg.split(' ');
      switch(command[0]){
        case 'weather':
          send.msg = 'Zmiana pogody dla zalogowanych graczy';
          io.to(`map_${player.champion.map}`).emit("weather", command[1]);
          break;
        case 'kick':
          const to = players.find((p) => p.champion.name == command[1]);
          if(to){
            send.msg = `Gracz ${command[1]} został wyrzucony`;
            to.socket.disconnect();
          }
          break;
        case 'debugThrow':
          throw 'debugThrow';
          break;
        case 'stop':
          stopServer();
          break;
        default:
          send.msg = command[0]+' command not found';
          break;
      }
    }else{
      send.msg = 'Brak uprawnień';
    }
    socket.emit('chat', send);
  });
  socket.on("disconnect", function (msg) {
    const player = players.find((p) => p.socket == socket);
    if (player) {
      //save position
      sql.query(
        `UPDATE champions SET map=${player.champion.map}, position=Point(${Math.ceil(player.champion.position.x / 32)},${Math.ceil(player.champion.position.y / 32)}), lastOnline=CURRENT_TIMESTAMP(), session=null WHERE session='${player.champion.session}'`,
        function (err, result, fields) {
          if (err) console.log(err);
        }
      );
      const interval = intervals.find((i) => i.player == player);
      const intervalIndex = intervals.findIndex((i) => i.player == player);
      intervals.splice(intervalIndex, 1);
      clearInterval(interval.interval);

      console.log(
        `Player ${player.champion.name}(${player.champion.id}) loged out, saveing changes!`
      );
      const playerIndex = players.findIndex((p) => p.socket == socket);
      players.splice(playerIndex, 1);
    }
  });
});

http.listen(port, function () {
  console.log("listening on *:" + port);
});
const appServer = app.listen(port+1, function() {
  console.log("listening on *:" + (port+1));
});
app.get('/', function(req, res) {
  res.send('null');
});
app.get('/playerList', function(req, res) {
  const list = players.map((a) => a.champion.id);
  res.send(JSON.stringify(list));
});
app.get('/playerPos', function(req, res) {
  try{
    const id = req.query.id;
    const player = players.find((p) => p.champion.id == id);
    const send = {
      position: player.champion.position,
      errorCode: 0
    }
    res.send(JSON.stringify(send));
  }catch(e){
    const send = {
      position: null,
      errorCode: 1,
      error: 'Invalid request'
    }
    res.send(JSON.stringify(send));
  }
});

app.get('/command', function(req, res) {
  try{
    const c = req.query.c;
    switch(c){
      case 'stop':
        res.send('Stoping serwer');
        res.end();
        stopServer();
        break;
      default:
        res.send('command '+c+' not found!');
        break;
    }
  }catch(e){
    res.send('Invalid request');
  }
});
const sql = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "qsxd2hll",
  database: "raweng",
  insecureAuth: true,
});

sql.connect(function (err) {
  if (err) throw err;
});